# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/test/linux/simple_test/simple_test.c" "/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/test/linux/simple_test/CMakeFiles/simple_test.dir/simple_test.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/CMakeFiles/soem.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../lib/SOEM1.3.0/soem"
  "../lib/SOEM1.3.0/osal"
  "../lib/SOEM1.3.0/osal/linux"
  "../lib/SOEM1.3.0/oshw/linux"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
