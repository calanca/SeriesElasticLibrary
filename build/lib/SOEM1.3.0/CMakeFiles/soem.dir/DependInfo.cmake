# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/osal/linux/osal.c" "/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/CMakeFiles/soem.dir/osal/linux/osal.c.o"
  "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/oshw/linux/nicdrv.c" "/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/CMakeFiles/soem.dir/oshw/linux/nicdrv.c.o"
  "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/oshw/linux/oshw.c" "/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/CMakeFiles/soem.dir/oshw/linux/oshw.c.o"
  "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatbase.c" "/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/CMakeFiles/soem.dir/soem/ethercatbase.c.o"
  "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatcoe.c" "/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/CMakeFiles/soem.dir/soem/ethercatcoe.c.o"
  "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatconfig.c" "/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/CMakeFiles/soem.dir/soem/ethercatconfig.c.o"
  "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatdc.c" "/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/CMakeFiles/soem.dir/soem/ethercatdc.c.o"
  "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatfoe.c" "/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/CMakeFiles/soem.dir/soem/ethercatfoe.c.o"
  "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatmain.c" "/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/CMakeFiles/soem.dir/soem/ethercatmain.c.o"
  "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatprint.c" "/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/CMakeFiles/soem.dir/soem/ethercatprint.c.o"
  "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatsoe.c" "/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/CMakeFiles/soem.dir/soem/ethercatsoe.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../lib/SOEM1.3.0/soem"
  "../lib/SOEM1.3.0/osal"
  "../lib/SOEM1.3.0/osal/linux"
  "../lib/SOEM1.3.0/oshw/linux"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
