# Install script for directory: /home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/andrea/workspace/SELib/SeaControl/install")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Debug")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/libsoem.a")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES
    "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatcoe.h"
    "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatdc.h"
    "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatbase.h"
    "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercat.h"
    "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatfoe.h"
    "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatprint.h"
    "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercattype.h"
    "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatsoe.h"
    "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatconfig.h"
    "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatmain.h"
    "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/soem/ethercatconfiglist.h"
    "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/osal/osal.h"
    "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/osal/linux/osal_defs.h"
    "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/oshw/linux/nicdrv.h"
    "/home/andrea/workspace/SELib/SeaControl/lib/SOEM1.3.0/oshw/linux/oshw.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/test/linux/slaveinfo/cmake_install.cmake")
  INCLUDE("/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/test/linux/eepromtool/cmake_install.cmake")
  INCLUDE("/home/andrea/workspace/SELib/SeaControl/build/lib/SOEM1.3.0/test/linux/simple_test/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

