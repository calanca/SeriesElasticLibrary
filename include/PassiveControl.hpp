#ifndef PASSIVECONTROL_H
#define PASSIVECONTROL_H

#include "ControlBase.hpp"
#include "AdaptiveControl.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/


/**
This class implements force control by using a #PID with integral roll off
*/
class PassivePIDControl: public SEControl
{
public:
	~PassivePIDControl(){delete iFilter;};
    PassivePIDControl(ISEHardware* hw);
    //double __process(double tau, double dtau, double dt);
    double ___process(double dt);

    double ki, kp, kd, ff;

protected:
    double err, derr, ierr;
	DigitalFilter* iFilter;
};

/**
This class implements a force control algorithm based on positive load accelleration feedback. It was first proposed in

[1] G. A. Pratt and M. M. Williamson, Series Elastic Actuators, in International Conference on Intelligent Robots and Systems, 1995, vol. 1, pp. 399-406.

*/
class PassivePrattForceControl: public SEControl
{
public:
	~PassivePrattForceControl(){};
    PassivePrattForceControl(double l1, double l2, ISEHardware* hw);
	double ___process(double deltaTime);
	double kb;
protected:
	void Log();
	double kp, kd, out, err, derr;
};


class MRPassivePrattForceControl: public PassivePrattForceControl, public ModelReference2
{
public:
	~MRPassivePrattForceControl(){}; //do do mi piacerebbe chiamare direttamente i distruttori
	MRPassivePrattForceControl(double l1, double l2, ISEHardware* hw);
//	MRPassivePrattForceControl(){};
    double ___process(double deltaTime);

protected:

};

/**
This class implements the force control algorithm proposed in

[1] H. Vallery, R. Ekkelenkamp, H. van der Kooij, and M. Buss, Passive and accurate torque control of series elastic actuators,2007 IEEE/RSJ Int. Conf. Intell. Robot. Syst., pp. 3534-3538, Oct. 2007.

it uses an inner motor velocity feedback
*/
class PassiveValleryForceControl: public PassivePIDControl
{
public:
	~PassiveValleryForceControl(){delete v;};
    PassiveValleryForceControl(ISEHardware* hw);
    double ___process(double dt);
	double kiv, kpv, kif, kpf, kdf;
protected:
	PID* v;

	double derr, err, ierr;
};




#endif // PASSIVECONTROL_H
