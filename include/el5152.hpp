#ifndef EL5152_HPP
#define EL5152_HPP

/****************************************************************************
 * Copyright (C) 2015 Lorenzo Bertelli, Andrea Calanca
 * @author Lorenzo Bertelli, Andrea Calanca
 * @date june 2015
 ****************************************************************************/

#include <vector>
#include <algorithm>
#include <iostream>
#include <string>

#include "elXXXX.hpp"

// SOEM includes
extern "C" {
#include "ethercattype.h"
#include "nicdrv.h"
#include "ethercatbase.h"
#include "ethercatmain.h"
#include "ethercatcoe.h"
#include "ethercatfoe.h"
#include "ethercatconfig.h"
#include "ethercatprint.h"
}

using namespace std;

class EL5152 : public ELXXXX
{
    public:
        EL5152(int slaveID, ec_slavet * ec_slave);
        int getPosition1();
        int getPosition2();
        unsigned int getPeriod1();
        unsigned int getPeriod2();
        bool resetEncoders();
};

#endif
