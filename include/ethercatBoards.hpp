#ifndef ETHERCAT_BOARDS_HPP
#define ETHERCAT_BOARDS_HPP

/****************************************************************************
 * Copyright (C) 2015 Lorenzo Bertelli, Andrea Calanca
 * @author Lorenzo Bertelli, Andrea Calanca
 * @date june 2015
 ****************************************************************************/

#include "el4004.hpp"
#include "el5152.hpp"
#include "el3102.hpp"
#include "el2202.hpp"
#include "el3102.hpp"
#include "el1202.hpp"


#include <sstream>
#include <cstring>
#include <vector>

using namespace std;

/**
This class comunicates via ethercat with the following Beckoff modules built in a custom board we called "sheepBoard"

EL5152 (2 encoder inputs) used to read the two encoders
EL4004 (2 analog output) used to  write current set point
EL3102 (2 analog inputs) used to read current + torque sensor
EL2202 (4 digital outputs) used to enable the current amplifier;
EL1202 (4 digital inputs) used to read the encoder Z index;

Max communication frequency: 3 KHz

The ethercat communication protocol is is based on the "Simple Open EtherCAT Master" (soem) project http://sourceforge.net/projects/soem.berlios/.

*/
class SheepBoard
{

    private:
        // Ethernet device used by EtherCAT master
        const char * ifname;

        // Input/Output EtherCATmapping
        char IOmap[4096];

        // EtherCAT modules
        EL2202 * digitalOutputs;
        EL1202 * digitalInputs;
        EL5152 * encoders;
        EL4004 * analogOutputs;
        EL3102 * analogInputs;

    public:
        SheepBoard(string const& name);

        bool configureHook();

        bool startHook();

        void updateHook();

        void stopHook();

        void cleanupHook();

        vector<double> motorsVoltageOut;
        vector<int> encodersPositionIn;
        vector<int> encodersPeriodIn;
        vector<double> analogSensorIn;
        vector<bool> digitalSensorIn;

};

#endif
