#ifndef EL2202_HPP
#define EL2202_HPP
#include "elXXXX.hpp"

// SOEM includes
extern "C" {
#include "ethercattype.h"
#include "nicdrv.h"
#include "ethercatbase.h"
#include "ethercatmain.h"
#include "ethercatcoe.h"
#include "ethercatfoe.h"
#include "ethercatconfig.h"
#include "ethercatprint.h"
}

/****************************************************************************
 * Copyright (C) 2015 Lorenzo Bertelli, Andrea Calanca
 * @author Lorenzo Bertelli, Andrea Calanca
 * @date june 2015
 ****************************************************************************/

using namespace std;

class EL2202 : public ELXXXX
{
    public:
        EL2202(int slaveID, ec_slavet * ec_slave);

        void setOutput(int channel, bool value);
        void initOutput();
};

#endif
