#ifndef FORCETASK_H
#define FORCETASK_H

#include <math.h>
#include <sstream>
#include <string>

#include"defines.h"
#include"LoopTask.hpp"
#include"AdaptiveControl.hpp"
#include"MotorControl.hpp"


/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/




/**
This class implements a task to test force control algorithms
*/
class ForceTask : public LoopTask
{
public:
    ForceTask(ISEHardware* hw);
    int _loop();
    void LogOpen();
    void Log();
	double position_ref;
	double log1, log2, log3;

	MotorPID* pd;
	DOBForceControl *dob;
	KEAdaptiveForceControl* mrac;
	KAdaptiveForceControl* kac;

	MotorControlBase* ctr;
	double k_des, d_des;
};


#endif // FORCETASK_H
