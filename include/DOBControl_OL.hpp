#ifndef DOBCONTROL_OL_H
#define DOBCONTROL_OL_H


#include "ControlBase.hpp"
#include "AnalogFilter.hpp"


/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Davide Ciocca, Andrea Calanca
 * @date Oct 2016
 ****************************************************************************/

/**
Base class for DOB force control of Series Elastic Actuators.
*/
class DOBControlBase : public SEControl
{
    public:
        double KP, KD, FF;
        bool dynamicFFEnabled;
        string name;

        DOBControlBase(double KP, double KD, double QcutoffHz, ISEHardware *hw);

        ~DOBControlBase() {
            delete Q_filter;
            delete PninvQ_filter;
            delete PninvQ_filterFF;
        }

        void Log();
        void LogOpen();

    protected:
        double QcutoffHz;
        double xi_q, w_q, w_q2;
        double dm, de;

        double *a_Q, *b_Q;
        double *a_PninvQ, *b_PninvQ;
        AnalogFilter *Q_filter, *PninvQ_filter, *PninvQ_filterFF;

        double d_hat;

};


/**
DOB force controller for Series Elastic Actuators. It implemants the open loop configuration proposed in [1] considering a second order nominal model which account for the motor dynamics only

[1]
*/
class DOBControl_OL : public DOBControlBase {
    public:
        DOBControl_OL(double KP, double KD, double QcutoffHz, ISEHardware *hw);
        double ___process(double deltaTime);

    private:
        double Ud, Ud_prev, err, derr;

};


/**
DOB force controller for Series Elastic Actuators. It implemants the open loop configuration proposed in [1] considering a third order nominal model which account for the motor dynamics and the link inertia

[1]
*/
class DOBControl_OL3order : public DOBControlBase
{
    public:

        DOBControl_OL3order(double KP, double KD, double QcutoffHz, ISEHardware *hw);
        ~DOBControl_OL3order() {  delete PninvQ_FFfilter;   }

        double ___process(double deltaTime);

    private:
        AnalogFilter *PninvQ_FFfilter;
        double Ud, d_hat, err, derr;
        double Ud_prev;

};

#endif // DOBCONTROL_OL_H
