#ifndef DOBCONTROL_CL_H
#define DOBCONTROL_CL_H


#include "ControlBase.hpp"
#include "AnalogFilter.hpp"
#include "DOBControl_OL.hpp"

#include <sstream>
#include <string>

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Davide Ciocca, Andrea Calanca
 * @date Oct 2016
 ****************************************************************************/

/**
DOB force controller for Series Elastic Actuators. It implemants the closed loop configuration proposed in [1] considering a second order nominal model which account for the motor dynamics only

[1]
*/
class DOBControl_CL : public DOBControlBase {
    public:
        DOBControl_CL(double KP, double KD, double QcutoffHz, ISEHardware *hw);
        double ___process(double deltaTime);

    private:
        double d_hat, err, derr, rd_prev;
};


/**
DOB force controller for Series Elastic Actuators. It implemants the closed loop configuration proposed in [1] considering a third order nominal model which account for the motor dynamics and the link inertia

[1]
*/
class DOBControl_CL3order : public DOBControlBase
{
    public:
        DOBControl_CL3order(double KP, double KD, double QcutoffHz, ISEHardware *hw);
        double ___process(double deltaTime);

    private:
        double rd_prev;
        double U, d_hat, err, derr;

};

#endif // DOBCONTROL_CL_H
