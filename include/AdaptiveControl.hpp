#ifndef ADAPTIVECONTROL_H
#define ADAPTIVECONTROL_H

#include "ControlBase.hpp"
#include "DigitalFilter.hpp"

#include <sstream>
#include <string>
#include <Eigen/Core>

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

/**
This class implements a 2nd order reference model:  ddx + l1*dx +l2*x = l2*r
where r is the input reference and x is the model output
*/
class ModelReference2
{
public:
	~ModelReference2(){};
    ModelReference2(double l1, double l2);

    //to be set at the beginning
    double l1, l2;

protected:
    void MRUpdate(double ref, double dt);

    //model reference variables
    double f1, f2, xr1, xr2, prev_xr1, prev_xr2;

};

/**
This is a base class for adaptive control algorithms. It implements a general adaptive control policy and the sliding-mode robustification proposed in

A. Calanca and P. Fiorini “'Understanding Environment Adaptive Control of Series Elastic Actuators'
*/
class AdaptiveForceControlBaseBC : public SEControl, public ModelReference2
{
public:
    AdaptiveForceControlBaseBC(double l1, double l2, ISEHardware* hw);
    double ___process(double deltaTime);

    virtual void adaptationUpdate(double dt)=0;
	virtual void setAdaptationSpeed(double g){G = g;};


	bool adaptationEnabled;
	bool referenceModelEnabled;

	double L; ///< algorithm convergence rate. Usually the higher the better. In the MRAC case this parameter does not influence either control bandwidth nor adaptation speed. It influences the precision -> this is shown in “Human-Adaptive Control of Series Elastic Actuators,” Robotica, vol. 2, no. 08, pp. 1301–1316, 2014. Some unstability can be seen for very low setting

	double b_est; ///< estimate of the parameter b
	double c_est; ///< estimate of the parameter c

    double ni; ///< sliding-mode gain, 0 by default
    double phi; ///< sliding boundary width

    double ka; ///< acceleration feedback gain, 0 by default


	double thetaA, dthetaA;
	double errA, yA;
	double wA;
	DigitalFilter *wAfilt, *yAfilt;

protected:

	double a_est, da_est;

	double G; ///< adaptation speed
    double us;

	void Log();

    double x_tilde, dx_tilde, nu, s;
    double db_est, dc_est;
    double log1, log2, log3;
};

/**
This class implements the human-adaptive control law proposed in

A. Calanca and P. Fiorini, 'Human-Adaptive Control of Series Elastic Actuators', Robotica, vol. 2, no. 08, pp. 1301–1316, 2014.

to control the SEA force/torque
*/
class MRAdaptiveForceControlBC: public AdaptiveForceControlBaseBC
{
public:
    ~MRAdaptiveForceControlBC(){};
    MRAdaptiveForceControlBC(double l1, double l2, ISEHardware* hw) : AdaptiveForceControlBaseBC(l1,l2,hw){};
	void adaptationUpdate(double dt);

};

/**
This class implements the indirect adaptive control law proposed in

A. Calanca, R. Muradore, and P. Fiorini, 'A Study on the Passivity Human-Adaptive Control of Elastic Actuators', Submitt. to IJRR.

to control the SEA force/torque.
*/
class IndirectAdaptiveForceControlBC: public AdaptiveForceControlBaseBC
{

public:
    ~IndirectAdaptiveForceControlBC(){delete w1filt; delete w2filt; delete w0filt;};
    IndirectAdaptiveForceControlBC(double l1, double l2, ISEHardware* hw);
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    void adaptationUpdate(double dt);
    bool gradientUpdate; ///< set the gradient (true) or recursive least square (false) update
	void setAdaptationSpeed(double G);
protected:
    void passivityConstraint();

	double w0, w1, w2;
	double err, y;
	double lambda, g;
	double kp, kd, dkp, dkp_max;

	Eigen::Vector2d theta, dtheta, w;
	Eigen::Matrix2d P, dP, rho, Q;

	DigitalFilter *w0filt, *w1filt, *w2filt;
};

/**
This class implements a multi model indirect adaptive control law to control a SEA force/torque. The control law is similar to the class IndirectAdaptiveForceControl but it takes advantages of multiple model of the human/enviroment. Thus the adaptation transients are very fast as the controller swtches to the model that gives the lower prediction error.
*/
class MultiAdaptiveForceControlBC: public IndirectAdaptiveForceControlBC
{

public:
    ~MultiAdaptiveForceControlBC(){ delete modelFilt; };
    MultiAdaptiveForceControlBC(double l1, double l2, ISEHardware* hw);
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    void adaptationUpdate(double dt);

protected:

	double err1, err2, switc;
	DigitalFilter *modelFilt;

	Eigen::Vector2d theta1, dtheta1;
	Eigen::Matrix2d P1, dP1;

	Eigen::Vector2d theta2, dtheta2;
	Eigen::Matrix2d P2, dP2;

};

class MRPDControl : public SEControl, public ModelReference2
{
public:
	~MRPDControl(){delete iFilter;};
    MRPDControl(double l1, double l2, ISEHardware* hw);
    double ___process(double dt);

	double kp, kd, ki;
protected:
	void Log();
	double  out, err, derr, ierr;
	DigitalFilter* iFilter;
};

#endif // ADAPTIVECONTROL_H
