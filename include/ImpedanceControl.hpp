#ifndef SEAIMPEDANCECONTROL_H
#define SEAIMPEDANCECONTROL_H

#include "ControlBase.hpp"
#include "SlidingMode.hpp"
#include "AdaptiveControl.hpp"
#include "PassiveControl.hpp"
#include "AnalogFilter.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

/**
Base class for impedance control algorithms. It provides desired stifness and damping proprierties and common log fonctionality for child impedance control algorithms (common file naming and data streaming)
*/
class ImpedanceControlBase: public SEControl
{
	public:
		ImpedanceControlBase(ISEHardware* hw);
		void Log();
		double k_des; ///< desired stiffness
		double d_des; ///< desired damping
		void LogOpen();
    protected:
		double log1, log2, log3;
};

/**
Under test - works only for direct drive motor
*/
class TrivialImpedanceControl: public ImpedanceControlBase
{
	public:
		TrivialImpedanceControl(ISEHardware* hw) : ImpedanceControlBase(hw) {}
		double ___process(double dt);
	private:
};

/**
This class implements impedance control based on an inner PD force control, using the PID class.
*/
class BasicImpedanceControl: public ImpedanceControlBase
{
	public:
//		BasicImpedanceControl(ISEHardware* hw) : ImpedanceControlBase(hw), pid(0,10,1) { 	this->filename = "BIC-PDlow"; };
        BasicImpedanceControl(ISEHardware* hw) : ImpedanceControlBase(hw), pid(0,80,1.5) { 	this->filename = "BIC-PDhigh"; };
//		BasicImpedanceControl(ISEHardware* hw) : ImpedanceControlBase(hw), pid(0,50,1) { 	this->filename = "BIC-PDmid"; };
		double ___process(double dt);
	PID pid;
};


/**
This class implements impedance control based on an inner PD force control, using the PID class.
*/
class CollocatedImpedanceControl: public ImpedanceControlBase
{
	public:
//		CollocatedImpedanceControl(ISEHardware* hw) : ImpedanceControlBase(hw), pid(0,10,1) { 	this->filename = "CIC-PDlow"; };
//		CollocatedImpedanceControl(ISEHardware* hw) : ImpedanceControlBase(hw), pid(0,50,1) { 	this->filename = "CIC-PDhigh"; };
		CollocatedImpedanceControl(ISEHardware* hw) : ImpedanceControlBase(hw), pid(0,20,1) { 	this->filename = "CIC-PDmid"; };
		double ___process(double dt);
	PID pid;
	private:
	double k_des_m;
};


/**
This class implements impedance control based on an inner PD force control, using the PID class.
*/
class CollocatedAdmittanceControl: public ImpedanceControlBase
{
	public:
        CollocatedAdmittanceControl(ISEHardware* hw) : ImpedanceControlBase(hw), pid(0,100,1)
        {
            this->filename = "CAC-PDhigh";
            A_over_s_ref = NULL;
            A_over_s_dref = NULL;
        };
//		CollocatedAdmittanceControl(ISEHardware* hw) : ImpedanceControlBase(hw), pid(0,50,1) { this->filename = "CAC-PDmid"; };
		double ___process(double dt);
    AnalogFilter *A_over_s_ref;
    AnalogFilter *A_over_s_dref;
	PID pid;
};


/**
This class implements impedance control based on an inner adaptive force control.
*/
class AdaptiveImpedanceControl: public ImpedanceControlBase
{
	public:
		AdaptiveImpedanceControl(ISEHardware* hw); ///< Edit here to set the default adaptive algorithm
		double ___process(double dt);
		AdaptiveForceControlBaseBC* c;

};

/**
This class implements impedance control based on an inner sliding-mode force control
*/
class SMImpedanceControl: public ImpedanceControlBase
{
	public:
		SMImpedanceControl(ISEHardware* hw); ///< Edit here set the specific SM algorithm
		double ___process(double dt);
		SlidingModeForceControlBase* c;
};


/**
This class implements impedance control based on an passive force control with inner motor velocity feedback. Passivity analisis of this algorithm is reported in

H. Vallery, J. Veneman, E. H. F. van Asseldonk, R. Ekkelenkamp, M. Buss, and H. van Der Kooij, "Compliant actuation of rehabilitation robots", IEEE Robot. Autom. Mag., vol. 15, no. 3, pp. 60-69, Sep. 2008.
*/
class VelocitySourcedImpedanceControl: public ImpedanceControlBase
{
	public:
		VelocitySourcedImpedanceControl(ISEHardware* hw);
		double ___process(double dt);
		PassiveValleryForceControl* c;
};


#endif // SEAIMPEDANCECONTROL_H
