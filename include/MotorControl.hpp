#ifndef MOTORCONTROL_H
#define MOTORCONTROL_H

#include"defines.h"
#include"AdaptiveControl.hpp"


// cerca di identificare sia k che theta_e
// tenere Motorcontrol base?
class KEAdaptiveForceControl: public SEControl, public ModelReference2
{
public:
	KEAdaptiveForceControl(ISEHardware* hw);
	~KEAdaptiveForceControl(){};

	PID* pid;

    double ___process(double dt);

    virtual void adaptationUpdate(double dt);

	virtual void setAdaptationSpeed(double g){G = g;};

	bool adaptationEnabled;

	double a_est; ///< estimate of the parameter a
	double ddtheta0_est; ///< estimate of the parameter ddtheta0

  	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    bool gradientUpdate; ///< set the gradient (=true) or recursive least square (=false) update

protected:
	double G; ///< adaptation speed

    double da_est;

	double w0, w1, w2;
	double err, y;
	double lambda, g;

	Eigen::Vector2d theta, dtheta, w;
	Eigen::Matrix2d P, dP, rho, Q;

	DigitalFilter *w0filt, *w1filt, *w2filt;
	DigitalFilter  *ddtheta0Filter;

};

//identifica solo k
class KAdaptiveForceControl: public SEControl
{
public:
	KAdaptiveForceControl(ISEHardware* hw);
	~KAdaptiveForceControl(){};

	PID* pid;

    double ___process(double dt);

    virtual void adaptationUpdate(double dt);

	virtual void setAdaptationSpeed(double g){G = g;};

	bool adaptationEnabled;

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    bool gradientUpdate; ///< set the gradient (=true) or recursive least square (=false) update

protected:
	double G; ///< adaptation speed

	void Log();

    double log1, log2, log3;

	double w;
	double err, y;
	double lambda, g;

	double  theta, dtheta;
	double  P, dP, rho, Q;

	DigitalFilter *w0filt, *w1filt, *w2filt;
	DigitalFilter  *ddtheta0Filter;

};

/**
This class implements the DOB-based force control law proposed in

T. Murakam, F. Yu, and K. Ohnishi, “Torque Sensorless Control in Multidegree-of-Freedom Manipulator,” IEEE Trans. Ind. Electron., vol. 40, no. 2, pp. 259–265, 1993.

*/
class DOBForceControl: public SEControl
{
public:
	DOBForceControl(ISEHardware* hw);
	~DOBForceControl();

	double KP; ///< proportional force feedback gain (sensorless)

    double ___process(double dt);

protected:

    double Gdob; ///< bandwidth of the inner disturbance observer
    double Gobs; ///< bandwidth of the torque observer

	double dob, obs;

	DigitalFilter *dobFilter, *obsFilter;
};




//class KEAdaptiveForceControl: public MotorControlBase, public ModelReference2
//{
//public:
//	KEAdaptiveForceControl(double l1, double l2, ISEHardware* hw);
//	~KEAdaptiveForceControl(){};

//    double __process(double theta, double dtheta, double dt);

//    virtual void adaptationUpdate(double dt);
//
//	virtual void setAdaptationSpeed(double g){G = g;};
//
//	bool adaptationEnabled;
//	bool referenceModelEnabled;
//
//	double L; ///< algorithm convergence rate. Usually the higher the better. In the MRAC case this parameter does not influence either control bandwidth nor adaptation speed. It influences the precision -> this is shown in “Human-Adaptive Control of Series Elastic Actuators,” Robotica, vol. 2, no. 08, pp. 1301–1316, 2014. Some unstability can be seen for very low setting
//
//	double a_est; ///< estimate of the parameter a
//	double ddtheta0_est; ///< estimate of the parameter ddtheta0
//
//    double ni; ///< sliding-mode gain
//    double phi; ///< sliding boundary width
//
//
//protected:
//	double G; ///< adaptation speed
//    double us;
//
//	void Log();
//
//    double x_tilde, dx_tilde, nu, s;
//    double da_est;
//    double log1, log2, log3;
//};

#endif // MOTORCONTROL_H
