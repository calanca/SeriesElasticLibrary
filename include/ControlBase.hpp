#ifndef CONTROLLER_H
#define CONTROLLER_H

#include"defines.h"
#include <math.h>
#include <iostream>
#include <sstream>
#include <string.h>
#include "DigitalFilter.hpp"
#include <fstream>
#include "ISEHardware.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2016
 ****************************************************************************/

using namespace std;

/**
The class provides an cycle by cycle automatic logging functionality. Child class just need to override the member log() where the desred variable can be streamed to the iosteam logfile.
*/
class Loggable
{
public:
    bool logEnabled;  ///< Enables the log. Better to set after filling the class properties (the file name could contain their values)
	string filename;

	Loggable() { logEnabled = false;}
    virtual ~Loggable() {}

    /**
    it can be overridden if some particular log initialization is necessary e.g. log headers or filename may depend on class parameters
    */
    virtual void LogOpen()
    {
        string logPath;
        logPath = LOG_DIRECTORY + filename;
        if (logEnabled && !logfile.is_open())
            logfile.open(logPath.c_str());
    }

    void logClose() { if (logfile.is_open()) logfile.close();}
    virtual void Log() {} ///< to be overridden!!


protected:
	ostringstream filenameStream;
	ofstream logfile;///< data must be stremed to this object when overriding the virtual method Log()

};

/**
This virtual class describes a generic Controller. The control law should be implemented by childs in the virtual _process function. The class also provides an automatic logging functionality by overriding the member log.
*/
class ControlBase: public Loggable
{
public:
    virtual ~ControlBase() {}

	virtual double _process(double theta, double dtheta, double dt)=0;
    double process(double theta, double dtheta, double dt)
    {
    	this->time += dt;
		ret = _process(theta,dtheta,dt);
    	if (logEnabled && !logfile.is_open()) LogOpen();
		if (logEnabled) Log();
    	return ret;
	}

    double ref, dref, ddref;

    void refSaturation( double lowThreshold, double upThreshold);

    inline static double saturation(double in, double threshold);
    inline static double saturation(double in,  double lowThreshold, double upThreshold);

protected:
	double time; ///< the (real-) time variable automatically updated every dt
    double out; ///< just for child convenience

private:
	double ret;
};

/**
This virtual class describes a controller for rigid joint i.e. a motro joint. The control law should be implemented by childs in the virtual __process function. The class provides motor position, velocity and torque members and an additional torque observer in the case the the torque sensor is missing. Also it provides friction compensation utilities.
*/
class MotorControlBase: public ControlBase
{
public:
	MotorControlBase(IMotorHardware *hw); ///< constructor. By default  accellerations are filtered (50Hz at Ts=0.333ms) while position and velocityes are not processed
    virtual ~MotorControlBase();

	virtual double __process(double theta, double dtheta, double dt)=0;  ///< virtual method that implements the specific MOTOR control law

    double _process(double theta, double dtheta, double dt); ///< reads the sensor data from IMotorHardware and sets their pre-processing, then invokes __process. Modify this method for changing the pre-processing. The frist two parameters (theta and dtheta) are not used because the sensors are directly read from IMotorHardware.

    IMotorHardware *hw; ///< where to retrieve the sensor data

	double Kt; ///< motor torque constant (just for ease of use !!)
	double Jm; ///< reflected motor inertia, including the gear ratio. (just for ease of use !!)

    double theta_m, dtheta_m, ddtheta_m, jerk_m; ///< for ease of use
	double tau, dtau, ddtau; ///< for ease of use

	double torqueObserver, diffTorqueObserver;

    DigitalFilter* thetaMFilter; ///< low-pass filter for thetaM, disabled (set to null) by default (one pole)
    DigitalFilter* dthetaMFilter; ///< low-pass filter for dthetaM, disabled (set to null) by default (one pole)
	DigitalFilter* ddthetaMFilter; ///< low-pass filter for ddthetaM, set to 50Hz bandwidth by default (one pole)
	DigitalFilter* torqueFilter; ///< low-pass filter for the torque read from the torque sensor, disabled (set to null) by default (one pole)
    DigitalFilter* dtorqueFilter; ///< low-pass filter for the numerical derivative of torque from torque sensor, disabled (set to null) by default (one pole)
    DigitalFilter* ddtorqueDifferentiatorFilter;///< differentiator filter with low pass stage for the numerical derivative of dtorque. Default bandwidth is 10Hz
    DigitalFilter* jerkMDifferentiatorFilter;///<< differentiator filter with low pass stage for the motor jerk. The bandwidth is 10Hz by default
	DigitalFilter* torqueObserverFilter; ///< preprocessing filter for the torque observer. The bandwidth is 5Hz by default

	double frictionCurrentCompensationColoumb(double velocity)
	{ return frictionTorqueCompensationColoumb(velocity) / Kt;}


    double frictionTorqueCompensationColoumb(double velocity)
    { return signbit(velocity) ? -hw->coulombFrictionN : hw->coulombFrictionP;}

    double frictionCurrentCompensation(double velocity)
	{ return frictionTorqueCompensation(velocity) / Kt;}

    double frictionTorqueCompensation(double velocity)
	{ return signbit(velocity) ? -hw->coulombFrictionN - hw->viscousFrictionN*fabs(velocity) : hw->coulombFrictionP + hw->viscousFrictionP*fabs(velocity);}

	double frictionCurrentCompensation()
	{ return frictionCurrentCompensation(dtheta_m);}

	double frictionTorqueCompensation()
	{ return frictionTorqueCompensation(dtheta_m);}


private:
    double torqueObserverCutOff, prev_torqueObserver, g;
};

//todo put something protected?

/**
This is the abstract base class for control algorithms for series elastic joints, namely SE (series elastic joint/actuators). It contains the basic SEA states including the spring torque (tau), the motor position (theta_m) and the environment position (theta_e) with first and second order derivatives. All these variables can be optionally filtered. This class retirieve the signal from the hw so it needs a pointer to an object which implements the ISEHardware interface.Child classes must override the member ___process
*/
class SEControl: public MotorControlBase
{
public:
    SEControl(ISEHardware *hw); ///< constructor. Accellerations and velocoties are filtered (50Hz) by default. positions are not processed
    virtual ~SEControl();

	virtual double ___process(double dt)=0; ///< virtual method that implements the specific SEA control law
	double __process(double theta, double dtheta, double dt); ///< reads the sensor data from ISEHardware and sets their pre-processing, then invokes ___process. Modify this method for changing the pre-processing. The frist two parameters (theta and dtheta) are not used because the sensor data is directly read from ISEHardware.

    ISEHardware *sehw; ///< a pointer to hw casted using the ISEHardware interface

    void Log();
    double log1, log2, log3, log4, log5;

	double A; ///< A = Jm / Kspring
	double Kspring; ///< SEA spring stiffness constant (for ease of use)

	double theta_e, dtheta_e, ddtheta_e; ///< for ease of use

	bool torqueFromDisplacement; ///< set true to overwrite the force sensor data


    DigitalFilter* thetaEFilter; ///< preprocessing filter for thetaE, disabled (set to null) by default
    DigitalFilter* dthetaEFilter; ///< preprocessing filter for dthetaE, disabled (set to null) by default
    DigitalFilter* ddthetaEFilter; ///< preprocessing filter for ddthetaE, set to 50Hz (at T=0.333ms) by default (low-pass, one pole)
};

//todo put something protected?


/**
A PID controller with a velocity filter an a output stage filter (before the control output)
*/
class PID: public ControlBase
{
public:
    PID(double ki, double kp, double kd);
    double _process(double theta, double dtheta, double deltaTime);
    double KP, KD, KI, integralSAT;

    DigitalFilter* outFilter; ///< output filter. Set to null (default) to skip it
    DigitalFilter* velFilter; ///< velocity filter. Set to null (default) to skip it

private:
    double err, derr, ierr;

};

/**
A PID algorithm for motor control. It uses the class PID by composition
*/
class MotorPID: public MotorControlBase
{
public:
    MotorPID(double ki, double kp, double kd, ISEHardware* hw);
    double __process(double pos, double vel, double dt);
    PID* pid;
};




#endif // CONTROLLER_H
