#ifndef EL4004_HPP
#define EL4004_HPP
#include "elXXXX.hpp"

/****************************************************************************
 * Copyright (C) 2015 Lorenzo Bertelli, Andrea Calanca
 * @author Lorenzo Bertelli, Andrea Calanca
 * @date june 2015
 ****************************************************************************/

// SOEM includes
extern "C" {
#include "ethercattype.h"
#include "nicdrv.h"
#include "ethercatbase.h"
#include "ethercatmain.h"
#include "ethercatcoe.h"
#include "ethercatfoe.h"
#include "ethercatconfig.h"
#include "ethercatprint.h"
}


using namespace std;

class EL4004 : public ELXXXX
{
    public:
        EL4004(int slaveID, ec_slavet * ec_slave);

        void setAbsAnalog1(double value);
        void setAbsAnalog2(double value);
        void setAbsAnalog3(double value);
        void setAbsAnalog4(double value);

        void setAnalog_1_2(double value);
        void setAnalog_3_4(double value);


    private:
        unsigned short scale(double value);

};

#endif
