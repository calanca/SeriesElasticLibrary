#ifndef SEFORCETASK_H
#define SEFORCETASK_H

#include <math.h>
#include <sstream>
#include <string>

#include "LoopTask.hpp"
#include"AdaptiveControl.hpp"
#include"SlidingMode.hpp"
#include"ImpedanceControl.hpp"
#include "PassiveControl.hpp"
#include "DOBControl_OL.hpp"
#include "DOBControl_CL.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

/**
This is an example class to test the implemented force and impedance control algorithms. Control algortithms are initialized in the init method and are asked to follow a torque reference in the _loop method.
*/
class SEForceTask : public LoopTask
{
public:
	~SEForceTask();
	SEForceTask(ISEHardware* hw);
    int _loop();
    double torque_ref;

protected:
    //controllers
    SEControl* ctr;

    MRAdaptiveForceControlBC* mrac;
    SlidingModeForceControl* sm;
	IntegralSlidingModeForceControl* ism;
    SuperTwistingForceControl* stw;
    MRPDControl* mrpd;
    PassivePIDControl* pd;

    TrivialImpedanceControl* imp;
    BasicImpedanceControl* BIC;
    VelocitySourcedImpedanceControl* VSIC;
    AdaptiveImpedanceControl* MRACimp;
    SMImpedanceControl* SMimp;
    CollocatedAdmittanceControl* CAC;
    CollocatedImpedanceControl* CIC;

    PassivePIDControl* passivePid;
    PassivePrattForceControl* passivePratt;
    MRPassivePrattForceControl* mrPassivePratt;
    PassiveValleryForceControl* passiveVallery;
    IndirectAdaptiveForceControlBC* indirectAdaptive;
    MultiAdaptiveForceControlBC* MMadaptive;
    DOBControl_OL* OL_dob;
    DOBControl_CL* CL_dob;
    DOBControl_OL3order* OL_dob_3_order;
    DOBControl_CL3order* CL_dob_3_order;

	double amp, freq;
    double k_des, d_des;

    void LogTorque();
	void init(double lambda1, double lambda2);
private:
};

#endif // SEFORCETASK_H
