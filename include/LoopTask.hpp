#ifndef LOOPTASK_H
#define LOOPTASK_H

#include "ISEHardware.hpp"
#include "ControlBase.hpp"
#include "DigitalFilter.hpp"
#include "SlidingMode.hpp"

#include <iostream>
#include <fstream>

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

using namespace std;

/**
This virtual class describes a task which is a constititive element of a task machine. The task is characterized by a loop operation, a next task (member LoopTask* next) and an after task (member LoopTask* after). The pointer next is initialized to "this" so the task loops by default. Within a loop the next task can be dynamically set. The task is also characterized by a maxDuration. When the duration expires the after task becomes the current task. If after or next are null the execution is stopped.

The correct usage of the task machine is:

@code
static LoopTask* currentTask; ///< The current task

int mainLoop() ... to be executed each dt seconds
{
    task = LoopTask::getCurrentTask();
    repeat = task->loop(dt);
}
@endcode

Note: A task can be considered similar a state of a finite state machine, however the transition policy is decided by the task itself and it is not centralized by a scheduler/dispatcher as in typical state machine design pattern. This mechanism is more generic because the transition can depend on specific informations which may not be included in the parent LoopTask class. A task is conceived as an high level operation which may require several information to do some reasoning and decide a transition. A FSM can be used within a single task to describe simpler operations.

Implementation details & suggestion:
1. The currentTask is a private static member of LoopTask. Childs cannot directly access it. They can just set the next and after tasks and invoke the reansitions goToNext or goToAfter
2. If the task are dynamically creates and not retrieved from a pool there may be a memory leak: all the previous states may remain in memory. Suggested solution: use a collection for the pool of task.
3. If the next state is changed you may want to dispose the previous task.
*/

class LoopTask : public Loggable
{
public:
	~LoopTask(){ delete hw; }; //do not delete next otherwise the next state will be erased!!
    LoopTask(ISEHardware* hw);

    int loop(double time);
    virtual int _loop(){ cout << "void task! \n"; return 0;};

    virtual bool isFinished() { return time>maxDuration; }; ///< the task termination condition, when it is satisfied the after task runs

    virtual void dispose() {};

    void timeReset() { time = 0; } ///< it resets the time

	LoopTask* next;
	LoopTask* after;

	double maxDuration; ///< defines the maximum duration of a task. If it is negative the task can be neverending. The duration is cumulative in case of loop-transitions unless the timeReset() is invoked

	void goToNext();
	void goToAfter();

	bool endLine; ///< if true a cout << endl is invoked every loop

	static LoopTask* getCurrentTask();
	static void setCurrentTask(LoopTask* t); //todo singletonize

protected:
    ISEHardware* hw;

    double dt, time;
    double current;  ///< the motor current [A]. Meant to be used in children. This is a motor control library.

    void LogAll();		///< logs all signals from ISEHardware in a csv file
	void LogCurPos();	///< logs all but the torque in a csv file
	void logMotor();	///< log only motor side variables in a csv file

private:
	bool toRet;
};

static LoopTask* currentTask; ///< The current task
LoopTask* create_task(ISEHardware* hw); ///< There must be defined somewere a create_task function that initializes the global variable currentTask


#endif // LOOPTASK_H
