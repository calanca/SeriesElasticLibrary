#ifndef HARDWARE_HPP
#define HARDWARE_HPP

#include "ethercatBoards.hpp"
#include "DigitalFilter.hpp"

#include <math.h>
#include <sstream>
#include <cstring>
#include <vector>

#include"defines.h"
#include"ISEHardware.hpp"

#define MOTOR 1
#define LOAD 0

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

using namespace std;

/**
This class implements the ISEHardware interface and comunicates with the SheepBoard.
Physical system parameters are defined in defines.h

*/
class SEHardware : public ISEHardware
{

	public:
        SEHardware(double Jm, double Kspring, double Kt);
        ~SEHardware(){delete envelopeFilterM; delete envelopeFilterE;}

        SheepBoard* sheepBoard;

        void refresh(double dt);

		void setCurrent(double value) { sheepBoard->motorsVoltageOut[0] = curSaturation(value) * SET_CURRENT_CONVERSION;}
        void setTauM(double value) { setCurrent(value/KT) ;}

        double getCurrent(){ return active_current; }
        double getTauM(){ return active_tau_m; }

        double getThetaM(){ return thetaM; }
        double getThetaE(){ return thetaE; }
        double getDThetaM(){ return dthetaM; }
        double getDThetaE(){ return dthetaE; }
        double getDiffThetaM(){ return diffthetaM; }
        double getDiffThetaE(){ return diffthetaE; }
        double getDDiffThetaM(){ return ddiffthetaM; }
        double getDDiffThetaE(){ return ddiffthetaE; }

		bool getIndexM(){ return sheepBoard->digitalSensorIn[1]; }
        bool getIndexE(){ return sheepBoard->digitalSensorIn[0]; }

        double getTorque(){ return torque;}
        double getDiffTorque(){ return difftorque;}

		void resetM(){
			zeroMOTOR = sheepBoard->encodersPositionIn[MOTOR]; prev_encoderMOTOR = zeroMOTOR;
			prev_thetaM = thetaM; envelopeFilterM->clean(); prev_diffthetaM = diffthetaM;
		}

		void resetE(){
			zeroLOAD = sheepBoard->encodersPositionIn[LOAD]; prev_encoderLOAD = zeroLOAD;
			prev_thetaE = thetaE; envelopeFilterM->clean(); prev_diffthetaM = diffthetaM;
			}

		bool isStandStill(int time);
        bool isSaturated(){return saturation_flag;}
		bool saturation_flag;

    private:
		int i;
		double rand_amp,randM,randE;

		double thetaM, prev_thetaM;
        double thetaE, prev_thetaE;
        double thetaMzero;
        double thetaEzero;

        int prev_encoderMOTOR, prev_encoderLOAD, zeroMOTOR, zeroLOAD;

        double dthetaM, diffthetaM, periodM, signM;
        double dthetaE, diffthetaE, periodE, signE;

        double dthetaM_envelope, prev_diffthetaM, period_dthetaM, prev_dthetaM, inc_dthetaM, ddiffthetaM;
        double dthetaE_envelope, prev_diffthetaE, period_dthetaE, prev_dthetaE, inc_dthetaE, ddiffthetaE;

        DigitalFilter* envelopeFilterM;
        DigitalFilter* envelopeFilterE;

        DigitalFilter* derivatorM;
        DigitalFilter* derivatorE;

        int isStandStillM, isStandStillE;

        double toRadians;

        double active_tau_m, active_current, torque, difftorque, prev_torque;

        double curSaturation(double in);

};

#endif
