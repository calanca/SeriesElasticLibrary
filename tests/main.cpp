#include <iostream>
#include <vector>
#include <cmath>
#include <signal.h>
#include <stdlib.h>
#include "SEHardware.hpp"
#include "LoopTask.hpp"
#include "defines.h"

//Real Time
#include <time.h>
#include <sched.h>

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

#define MY_PRIORITY     49 //kernel-like priority



using namespace std;

// Signal handler to manage the Exit
bool exitLoop = false;
void sighandler(int sig){exitLoop = true;}

LoopTask* task;


static inline void tsnorm(struct timespec *ts);
void safety(ISEHardware* hw);


int main(int argc,char** argv)
{
	srand (time(NULL));

	bool slaveConfigInit = false;
	double time,startTime,prev_time = 0;
	int repeat = 1;

    // REALTIME variables
    struct timespec t;
    struct sched_param param;

    // Declare ourself as REALTIME task
    param.sched_priority = MY_PRIORITY;
    if (sched_setscheduler(0, SCHED_FIFO, &param) == -1) {
        cerr << "sched_setscheduler failed" << endl;
        return -1;
    }

    signal(SIGABRT, &sighandler);
    signal(SIGTERM, &sighandler);
    signal(SIGINT, &sighandler);

    //Hardware init
    SEHardware* hw = new SEHardware(JM, K_SPRING, KT);
    hw->coulombFrictionP = COULOMB_FRICTION_POS;
    hw->coulombFrictionN = COULOMB_FRICTION_NEG;
    hw->viscousFrictionP = VISCOUS_FRICTION_POS;
    hw->viscousFrictionN = VISCOUS_FRICTION_NEG;
    hw->currentSaturation = CUR_SAT;

    //this method is supposed to configure the communication with the etercat hardware
    hw->sheepBoard->configureHook();
    hw->setCurrent(0.0);

    //if the configuration is ok, we setup/start the communication with the hardware
    while (!slaveConfigInit && !exitLoop) slaveConfigInit = hw->sheepBoard->startHook();

    //Main Loop

    LoopTask::setCurrentTask(create_task(hw));

	time = 0;

	clock_gettime(CLOCK_MONOTONIC,&t);
	t.tv_nsec += RT_INTERVAL;

    //this method retrieves/updates the data from/to the hardware
    hw->sheepBoard->updateHook();
    hw->resetM();
    hw->resetE();
    hw->refresh(0.0003);

	//retrieve the time
	clock_gettime(CLOCK_MONOTONIC,&t);
	startTime = (t.tv_sec + t.tv_nsec / NSEC_PER_SEC);
	t.tv_nsec += RT_INTERVAL;
	tsnorm(&t);
	clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL);

    while (!exitLoop && repeat)
    {
        // REALTIME: Wait until next shot
        clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL);
		//time [s] variable update
		time = (t.tv_sec + t.tv_nsec / NSEC_PER_SEC) - startTime;

        //THE TASK LOOP!!!
        task = LoopTask::getCurrentTask();
        repeat = task->loop(time-prev_time);
        prev_time = time;
		safety(hw);

 		// Perform the first update, to send the data to ethercat slaves
		hw->sheepBoard->updateHook();

        // REALTIME: Calculate delay
        t.tv_nsec += RT_ETHERCAT_DELAY;
        tsnorm(&t);
		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL);

		// Perform a second update, to get the slaves data
       	hw->sheepBoard->updateHook();

        // REALTIME: Calculate next shot
        t.tv_nsec += (RT_INTERVAL - RT_ETHERCAT_DELAY);
        tsnorm(&t);
    }

	task->logClose();
	task->dispose();
}

/** Euristic safety check */
void safety(ISEHardware* hw)
{
	static int unsafety;

	if(abs(hw->getDThetaE())> 200) unsafety++;

	if(unsafety > 1000)
	{
		exitLoop = 1;
		hw->setCurrent(0.0);
		cout << "Safety Limits Excedeed!!!" << endl;
	}
}


/** Normalize the nanoseconds if they are > 1 second */
static inline void tsnorm(struct timespec *ts)
{
    while (ts->tv_nsec >= NSEC_PER_SEC) {
        ts->tv_nsec -= NSEC_PER_SEC;
        ts->tv_sec++;
    }
}
