n = 4;

f = figure('Position', [0, 0, 800, 150*n]*1.2);
set(f,'PaperPositionMode','auto')

s1 = subplot(n,1,1);
plot(t,ref,t,tau)%,'LineWidth',2.0)
l1= legend('$\tau_r[Nm]$','$\tau_h[Nm]$');
% yl1 = ylabel('$torque  [Nm]$');
% axis([startTime endTime -0.5 0.5 ])
set(l1,'Interpreter','Latex');
% set(yl1,'Interpreter','Latex');

s2 = subplot(n,1,2);
plot(t,dtheta_m)
l2= legend('$\dot{\theta}_m$');
set(l2,'Interpreter','Latex');

% 
s3 = subplot(n,1,3);
plot(t,tau,t,observer)
l3 = legend('$\tau$','$\hat \tau$');
% axis([startTime endTime -1 1])
set(l3,'Interpreter','Latex');

% 
s4 = subplot(n,1,4);
plot(t,log2)
l4 = legend('$\hat{a}$');
% yl4 = ylabel('$[s]$');
% axis([startTime endTime 0 0.6])
set(l4,'Interpreter','Latex');
% set(yl4,'Interpreter','Latex');

xl = xlabel('$t[s]$');
set(xl,'Interpreter','Latex');

linkaxes([s1 s2 s3 s4],'x');


%%



% print(f,'-painters','-depsc',strrep(filelist(i).name,'csv','eps'))
% print(f1,'-painters','-depsc',strrep(filelist(i).name,'.csv','_energy.eps'))