clear

% filepath = 'Impedance0106/'
% files = dir([filepath 'adaptive*inf.csv']);
% % 
% % filepath = 'Impedance/'
% files = dir([filepath 'adaptive*.csv']);

filepath = '../../SeaControl/';
filename =  'Kadaptive';
files = dir([filepath filename '.csv']);

filelist = files;

for i=1:length(filelist)
% pause

filelist(i).name

fid = fopen([filepath filelist(i).name]); 
DATA = textscan(fid,'%f %f %f %f %f %f %f %f %f %f %f %f %f');

D = cell2struct(DATA(1,1),'A',1);
ref = struct2array(D);

D = cell2struct(DATA(1,2),'A',1);
dref = struct2array(D);

D = cell2struct(DATA(1,3),'A',1);
tau = struct2array(D);

D = cell2struct(DATA(1,4),'A',1);
dtau = struct2array(D);

D = cell2struct(DATA(1,5),'A',1);
theta_m = struct2array(D);

D = cell2struct(DATA(1,6),'A',1);
dtheta_m = struct2array(D);

D = cell2struct(DATA(1,7),'A',1);
theta_e = struct2array(D);

D = cell2struct(DATA(1,8),'A',1);
dtheta_e = struct2array(D);

D = cell2struct(DATA(1,9),'A',1);
observer = struct2array(D);

D = cell2struct(DATA(1,10),'A',1);
log1 = struct2array(D);

D = cell2struct(DATA(1,11),'A',1);
log2 = struct2array(D);

D = cell2struct(DATA(1,12),'A',1);
log3 = struct2array(D);

D = cell2struct(DATA(1,13),'A',1);
t = struct2array(D);

startTime = 0;
endTime = 15;

kpaperPlot

fclose(fid);
end



