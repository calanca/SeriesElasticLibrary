clear

% filepath = 'Impedance0106/'
% files = dir([filepath 'adaptive*inf.csv']);
% % 
% % filepath = 'Impedance/'
% files = dir([filepath 'adaptive*.csv']);

filepath = '../../SeaControl/';
filename =  'iadaptive';
files = dir([filepath filename '.csv']);

filelist = files;

for i=1:length(filelist)
% pause

filelist(i).name

fid = fopen([filepath filelist(i).name]); 
DATA = textscan(fid,'%f %f %f %f %f %f %f %f %f %f %f');

D = cell2struct(DATA(1,1),'A',1);
ref = struct2array(D);

D = cell2struct(DATA(1,2),'A',1);
tau = struct2array(D);

D = cell2struct(DATA(1,3),'A',1);
dtau = struct2array(D);

D = cell2struct(DATA(1,4),'A',1);
xr1 = struct2array(D);

D = cell2struct(DATA(1,5),'A',1);
xr2 = struct2array(D);

D = cell2struct(DATA(1,6),'A',1);
b = struct2array(D);

D = cell2struct(DATA(1,7),'A',1);
c = struct2array(D);

D = cell2struct(DATA(1,8),'A',1);
model = struct2array(D);

D = cell2struct(DATA(1,9),'A',1);
a_est1 = struct2array(D);

D = cell2struct(DATA(1,10),'A',1);
a_est = struct2array(D);

D = cell2struct(DATA(1,11),'A',1);
t = struct2array(D);

startTime = 0;
endTime = 15;

paperPlot

fclose(fid);
end



