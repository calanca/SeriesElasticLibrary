clear
close all
filepath = '../../SeaControl/';



files = dir([filepath 'force*.csv']);
filelist = files;

for i=1:length(filelist)
filelist(i).name
fid = fopen([filepath filelist(i).name]); 
DATA = textscan(fid,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f');

D = cell2struct(DATA(1,1),'A',1);
t = struct2array(D);

D = cell2struct(DATA(1,2),'A',1);
cur = struct2array(D);

D = cell2struct(DATA(1,3),'A',1);
cur_cmd = struct2array(D);

D = cell2struct(DATA(1,4),'A',1);
theta = struct2array(D);

D = cell2struct(DATA(1,5),'A',1);
dtheta = struct2array(D); %period

D = cell2struct(DATA(1,6),'A',1);
torque = struct2array(D);

D = cell2struct(DATA(1,7),'A',1);
dtorque = struct2array(D);

D = cell2struct(DATA(1,8),'A',1);
ref = struct2array(D);

D = cell2struct(DATA(1,9),'A',1);
dref = struct2array(D);

D = cell2struct(DATA(1,10),'A',1);
torqueObs = struct2array(D);

D = cell2struct(DATA(1,11),'A',1);
dtorqueObs = struct2array(D);

D = cell2struct(DATA(1,12),'A',1);
log1 = struct2array(D);

D = cell2struct(DATA(1,13),'A',1);
log2 = struct2array(D);

D = cell2struct(DATA(1,14),'A',1);
log3 = struct2array(D);

% I = find(abs(dtheta) < 1);
I = find(abs(dtorqueObs) > 200);


f = figure('Position', [0, 0, 800, 150]*1.2);
plot(t,ref,t,torque)
legend('ref','torque')



% n = 3;
% f = figure('Position', [0, 0, 800, 150*n]*1.2);
% set(f,'PaperPositionMode','auto')
% subplot(n,1,1)
% plot(t,cur*1.43,t,torque)
% legend('cur*kt','torque')
% subplot(n,1,2)
% plot(t,ref,t,torque)
% legend('ref','torque')
% subplot(n,1,3)
% plot(t,torqueObs,t,torque,t,dtheta/10)
% legend('obs','torque','vel')

fclose(fid);

end


