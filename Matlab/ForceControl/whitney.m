clear
close all
filepath = '../../SeaControl/';
filepath = '';


files = dir([filepath 'whitney*.csv']);
filelist = files;

for i=1:length(filelist)
filelist(i).name
fid = fopen([filepath filelist(i).name]); 
DATA = textscan(fid,'%f %f %f %f %f %f %f %f %f');

D = cell2struct(DATA(1,1),'A',1);
t = struct2array(D);

D = cell2struct(DATA(1,2),'A',1);
cur = struct2array(D);

D = cell2struct(DATA(1,3),'A',1);
cur_cmd = struct2array(D);

D = cell2struct(DATA(1,4),'A',1);
theta = struct2array(D);

D = cell2struct(DATA(1,5),'A',1);
dtheta = struct2array(D); %period

D = cell2struct(DATA(1,6),'A',1);
torque = struct2array(D);

D = cell2struct(DATA(1,7),'A',1);
dtorque = struct2array(D);

D = cell2struct(DATA(1,8),'A',1);
ref = struct2array(D);

D = cell2struct(DATA(1,9),'A',1);
dref = struct2array(D);

N = 2;
figure(1)
subplot(N,1,1)
plot(t,cur*1.43,t,torque)
legend('cur','torque')
subplot(N,1,2)
plot(t,ref,t,torque)
legend('ref','torque')


fclose(fid);

end


