clear
close all
filepath = '../../SeaControl/';



files = dir([filepath 'KEadaptive.csv']);
filelist = files;

for i=1:length(filelist)
filelist(i).name
fid = fopen([filepath filelist(i).name]); 
DATA = textscan(fid,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f');

D = cell2struct(DATA(1,1),'A',1);
t = struct2array(D);

D = cell2struct(DATA(1,2),'A',1);
cur = struct2array(D);

D = cell2struct(DATA(1,3),'A',1);
ctrl_out = struct2array(D);

D = cell2struct(DATA(1,4),'A',1);
theta_m = struct2array(D);

D = cell2struct(DATA(1,5),'A',1);
dtheta_m = struct2array(D); 

D = cell2struct(DATA(1,6),'A',1);
ddtheta_m = struct2array(D);

D = cell2struct(DATA(1,7),'A',1);
theta_e = struct2array(D);

D = cell2struct(DATA(1,8),'A',1);
dtheta_e = struct2array(D); 

D = cell2struct(DATA(1,9),'A',1);
ddtheta_e = struct2array(D);

D = cell2struct(DATA(1,10),'A',1);
tau = struct2array(D);

D = cell2struct(DATA(1,11),'A',1);
dtau = struct2array(D);

D = cell2struct(DATA(1,12),'A',1);
ddtau = struct2array(D);

D = cell2struct(DATA(1,13),'A',1);
ref = struct2array(D);

D = cell2struct(DATA(1,14),'A',1);
dref = struct2array(D);

D = cell2struct(DATA(1,15),'A',1);
ddref = struct2array(D);

D = cell2struct(DATA(1,16),'A',1);
log1 = struct2array(D);

D = cell2struct(DATA(1,17),'A',1);
log2 = struct2array(D);

D = cell2struct(DATA(1,18),'A',1);
log3 = struct2array(D);

D = cell2struct(DATA(1,19),'A',1);
log4 = struct2array(D);

D = cell2struct(DATA(1,19),'A',1);
log5 = struct2array(D);

k_env = log1;
theta_env = log2;
P_k_env = log3;
P_theta_env = log4;


n = 3;
f = figure('Position', [0, 0, 800, 150*n]*1.2);
set(f,'PaperPositionMode','auto')

subplot(n,1,1)
plot(t,ref,t,tau)
legend('ref','torque')


subplot(n,1,2)
plot(t,k_env)
legend('k env')

subplot(n,1,3)
plot(t,theta_env, t, theta_e)
legend('theta0')

fclose(fid);

end


