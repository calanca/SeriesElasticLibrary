clear
close all

filepath = '../Log/first/';
files = dir([filepath '*.csv']);

% filepath = 'Impedance7/kd/';
% files = dir([filepath '*.csv']);
% % 
% filepath = 'Impedance6/';
% files = dir([filepath 'CIC*d_des_0.1*.csv']);
% 
% filepath = 'PaperOvercoming/';
% files = dir([filepath '*d_des_0.05.csv']);

myaxis = [-1 1 -1.2 1.2];
printfigure = 0;

filelist = files;


for i=1:length(filelist)
    
filelist(i).name



fid = fopen([filepath filelist(i).name]); 
DATA = textscan(fid,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f');

D = cell2struct(DATA(1,1),'A',1);
ref = struct2array(D);

D = cell2struct(DATA(1,2),'A',1);
dref = struct2array(D);

D = cell2struct(DATA(1,3),'A',1);
ddref = struct2array(D);

D = cell2struct(DATA(1,4),'A',1);
tau = struct2array(D);

D = cell2struct(DATA(1,5),'A',1);
dtau = struct2array(D);

D = cell2struct(DATA(1,6),'A',1);
theta_m = struct2array(D);

D = cell2struct(DATA(1,7),'A',1);
dtheta_m = struct2array(D);

D = cell2struct(DATA(1,8),'A',1);
theta_e = struct2array(D);

D = cell2struct(DATA(1,9),'A',1);
dtheta_e = struct2array(D);

D = cell2struct(DATA(1,10),'A',1);
k_des = struct2array(D);

D = cell2struct(DATA(1,11),'A',1);
d_des = struct2array(D);

D = cell2struct(DATA(1,12),'A',1);
log1 = struct2array(D);

D = cell2struct(DATA(1,13),'A',1);
log2 = struct2array(D);

D = cell2struct(DATA(1,14),'A',1);
log3 = struct2array(D);

D = cell2struct(DATA(1,15),'A',1);
t = struct2array(D);

% 
% % startTime = 1;
% % endTime = 5;
% startTime = 0;
% endTime = t(end);
% 

ideal_tau = - theta_e*k_des(1) - dtheta_e*d_des(1);
ideal_dtheta = - tau/d_des(1);

f = figure;
set(f,'Position', [150, 400, 400, 400]);
set(f,'PaperPositionMode','auto')
plot(-ideal_tau,tau,-ideal_tau,ideal_tau,'r')
% title(filelist(i).name)
l1= legend('$\tau_e$','$\tau_{id}$');
xl1 = xlabel('$k_d \theta_e + d_d \dot{\theta}_e  [Nm]$');
yl1 = ylabel('$torque  [Nm]$');
axis(myaxis)

set(l1,'Interpreter','Latex');
set(xl1,'Interpreter','Latex');
set(yl1,'Interpreter','Latex');

tau_err = ideal_tau - tau;
theta_err = ideal_dtheta - theta_e;
impedance_err = (tau_err.*theta_err)./sqrt(tau_err.^2 + theta_err.^2);
impedance_err = impedance_err(~isnan(impedance_err));

avg_tau_err = sum(abs(tau_err))/length(tau);
avg_theta_err = sum(abs(theta_err))/length(tau);
avg_impedance_err = sum(abs(impedance_err))/length(tau);


max_tau_err = max(abs(tau_err));
max_theta_err = max(abs(theta_err));
max_impedance_err = max(abs(impedance_err));

if(printfigure==1)
print(1,'-painters','-deps2c',strrep(filelist(i).name,'csv','eps'))
end
pause
fclose(fid);
end
% 
% 
% 
