clear
filepath = '../Log/';

filelist = dir([filepath 'adaptive.csv']);

for i=1:length(filelist)
% pause

filelist(i).name

fid = fopen([filepath filelist(i).name]); 
DATA = textscan(fid,'%f %f %f %f %f %f %f %f %f %f %f');

D = cell2struct(DATA(1,1),'A',1);
ref = struct2array(D);

D = cell2struct(DATA(1,2),'A',1);
tau = struct2array(D);

D = cell2struct(DATA(1,3),'A',1);
dtau = struct2array(D);

D = cell2struct(DATA(1,4),'A',1);
xr1 = struct2array(D);

D = cell2struct(DATA(1,5),'A',1);
xr2 = struct2array(D);

D = cell2struct(DATA(1,6),'A',1);
b = struct2array(D);

D = cell2struct(DATA(1,7),'A',1);
c = struct2array(D);

D = cell2struct(DATA(1,8),'A',1);
model = struct2array(D);

D = cell2struct(DATA(1,9),'A',1);
y = struct2array(D);

D = cell2struct(DATA(1,10),'A',1);
L = struct2array(D);

D = cell2struct(DATA(1,11),'A',1);
t = struct2array(D);

startTime = 0;
endTime = t(end);

n = 4;

f = figure('Position', [0, 0, 800, 150*n]*1.2);
set(f,'PaperPositionMode','auto')

s1 = subplot(n,1,1);
plot(t,xr1,t,tau)%,'LineWidth',2.0)
l1= legend('$\tau_r[Nm]$','$\tau_h[Nm]$');
% yl1 = ylabel('$torque  [Nm]$');
axis([startTime endTime -0 1 ])
set(l1,'Interpreter','Latex');
% set(yl1,'Interpreter','Latex');


s2 = subplot(n,1,2);
plot(t,xr1-tau)
% yl2 = ylabel('$torque [Nm]$');
l2 = legend('$e[Nm]$');
axis([startTime endTime -0.1 0.1])
set(l2,'Interpreter','Latex');
% set(yl2,'Interpreter','Latex');

% 
s3 = subplot(n,1,3);
plot(t,c)
l3= legend('$\hat{c}$');
% axis([startTime endTime 0 1])
set(l3,'Interpreter','Latex');
% 
s4 = subplot(n,1,4);
plot(t,b)
l4 = legend('$\hat{b}$');
% yl4 = ylabel('$[s]$');
% axis([startTime endTime 0 0.01])
set(l4,'Interpreter','Latex');
% set(yl4,'Interpreter','Latex');

xl = xlabel('$t[s]$');
set(xl,'Interpreter','Latex');

linkaxes([s1 s2 s3 s4],'x');


fclose(fid);
end



