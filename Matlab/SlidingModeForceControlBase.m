clear
close all
matpath = 'Data\';
filepath = '../Log/';
%filepath = 'Csv\'

% files = dir([filepath 'sliding*2Hz-resonator.csv']);
% files = dir([filepath 'sliding*rect-wood-hand.csv']);
files = dir([filepath 'sliding.csv']);
% files = dir([filepath 'superTW.csv']);


filelist = files;

startTime = 0;
endTime = 30;

for i=1:length(filelist)
	
filelist(i).name

fid = fopen([filepath filelist(i).name]); 
DATA = textscan(fid,'%f %f %f %f %f %f %f %f %f');

D = cell2struct(DATA(1,1),'A',1);
t = struct2array(D);

D = cell2struct(DATA(1,2),'A',1);
err = struct2array(D);

D = cell2struct(DATA(1,3),'A',1);
derr = struct2array(D);

D = cell2struct(DATA(1,4),'A',1);
ref = struct2array(D);

D = cell2struct(DATA(1,5),'A',1);
dref = struct2array(D);

D = cell2struct(DATA(1,6),'A',1);
phi = struct2array(D);

D = cell2struct(DATA(1,7),'A',1);
ni= struct2array(D);

D = cell2struct(DATA(1,8),'A',1);
s = struct2array(D);

D = cell2struct(DATA(1,9),'A',1);
out = struct2array(D);


startTime = 0;
endTime = 30;
endTime = t(end);

f = figure('Position', [0, 0, 800, 150*4]*1.2);
set(f,'PaperPositionMode','auto')
% 
% figure(1)
% set(1,'Position',[0 0 1920 1080])

s1 = subplot(4,1,1);
%plot(t,ref,t,err+ref,t,err)
plot(s1,t,ref,t,err+ref)
legend('ref','tau')

s2 = subplot(4,1,2);
plot(s2,t,dref,t,derr+dref)
legend('dref','dtau')

s3 = subplot(4,1,3);
%plot(err,derr)
plot(s3,t,err)
% plot(s3,t,s)

% axis([0 20 -0.1 0.1])
legend('error')

s4 = subplot(4,1,4)
plot(s4,t,out/0.42375)
legend('current')

linkaxes([s1 s2 s3 s4],'x');


figure(2)

plot(t,s,t,10*ni,'r',t,phi,'g',t,-phi,'g')
legend('s','10ni','phi')

fclose(fid);
% pause


plen = 3000/4;
len = endTime*3000-1;
%err = tau-ref;
[rms s_rms max_value s_max_value] = errorPeriodAnalysis(err(2:end),len,plen)
end




