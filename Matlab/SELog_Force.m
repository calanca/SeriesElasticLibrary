clear
filepath = '../Log/';

filelist = dir([filepath 'PD.csv']);


for i=1:length(filelist)
filelist(i).name
fid = fopen([filepath filelist(i).name]); 
DATA = textscan(fid,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f');

D = cell2struct(DATA(1,1),'A',1);
t = struct2array(D);

D = cell2struct(DATA(1,2),'A',1);
ActiveCurrent = struct2array(D);

D = cell2struct(DATA(1,3),'A',1);
CurrentCommand = struct2array(D);

D = cell2struct(DATA(1,4),'A',1);
theta_m = struct2array(D);

D = cell2struct(DATA(1,5),'A',1);
dtheta_m = struct2array(D);

D = cell2struct(DATA(1,6),'A',1);
ddtheta_m  = struct2array(D);

D = cell2struct(DATA(1,7),'A',1);
theta_e = struct2array(D);

D = cell2struct(DATA(1,8),'A',1);
dtheta_e = struct2array(D);

D = cell2struct(DATA(1,9),'A',1);
ddtheta_e = struct2array(D);

D = cell2struct(DATA(1,10),'A',1);
tau = struct2array(D);

D = cell2struct(DATA(1,11),'A',1);
dtau = struct2array(D);

D = cell2struct(DATA(1,12),'A',1);
ddtau = struct2array(D);

D = cell2struct(DATA(1,13),'A',1);
ref = struct2array(D);

D = cell2struct(DATA(1,14),'A',1);
dref = struct2array(D);

D = cell2struct(DATA(1,15),'A',1);
ddref = struct2array(D);

startTime = 0;
endTime = t(end);

n = 2;

f = figure('Position', [0, 0, 800, 150*n]*1.2);
set(f,'PaperPositionMode','auto')

s1 = subplot(n,1,1);
plot(t,ref,t,tau)
l1= legend('$\tau_d(Nm)$','$\tau_s(Nm)$');
% yl1 = ylabel('$torque  (Nm)$');
axis([startTime endTime -0.0 1.5 ])
set(l1,'Interpreter','Latex');
% set(yl1,'Interpreter','Latex');

s2 = subplot(n,1,2);
plot(t,ref-tau)
% yl2 = ylabel('$torque (Nm)$');
l2 = legend('$e(Nm)$');
axis([startTime endTime -0.1 0.1])
set(l2,'Interpreter','Latex');
% set(yl2,'Interpreter','Latex');


xl = xlabel('$t \: (s)$');
set(xl,'Interpreter','Latex');

linkaxes([s1 s2],'x');


fclose(fid);
pause
end


