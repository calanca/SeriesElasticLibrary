clc;
clear;
clear global;
close all;


%%
filepath = '../../Log/';
FileList = dir([filepath, 'adaptive*.csv']);
N = size(FileList, 1);

for k = 1 : N
    filename = FileList(k).name;
    disp(filename);

    fid = fopen([filepath, filename]);
    DATA = textscan(fid, '%f %f %f %f %f %f %f %f %f %f %f');

    D = cell2struct(DATA(1, 1), 'A', 1);
    ref = struct2array(D);

    D = cell2struct(DATA(1, 2), 'A', 1);
    tau = struct2array(D);

    D = cell2struct(DATA(1, 3), 'A', 1);
    dtau = struct2array(D);

    D = cell2struct(DATA(1, 4), 'A', 1);
    xr1 = struct2array(D);

    D = cell2struct(DATA(1, 5), 'A', 1);
    xr2 = struct2array(D);

    D = cell2struct(DATA(1, 6), 'A', 1);
    b = struct2array(D);

    D = cell2struct(DATA(1, 7), 'A',1);
    c = struct2array(D);

    D = cell2struct(DATA(1, 8), 'A', 1);
    model = struct2array(D);

    D = cell2struct(DATA(1, 9), 'A', 1);
    y = struct2array(D);

    D = cell2struct(DATA(1, 10), 'A', 1);
    L = struct2array(D);

    D = cell2struct(DATA(1, 11), 'A', 1);
    t = struct2array(D);

    
    startTime = 0;
    endTime = 25;
    step = 5;
    ke_vect = [0.1, 0.2, 0.4, 0.8, 1.6];

    f = figure('Position', [10, 50, 600, 400] * 1.5);
    set(f, 'PaperPositionMode', 'auto')

    s1 = subplot(4, 1, 1);
    plot(s1, t, tau, 'b', t, xr1, 'r--')
    l1 = legend('$\tau_s\:[Nm]$', '$\tau_d\:[Nm]$');
    axis([startTime, endTime, 0.3, 0.85])
    set(l1, 'Interpreter', 'Latex');
    
    ystart = -0.08;
    yend = 0.1;
    hold on
    i = 1;
    for idx = 0 : step : endTime - step
        ke = ke_vect(i);
        plot([idx, idx], [ystart, yend], 'k', 'Linewidth', 1.2);
        label = text(idx + 0.58, -0.08, ['$k_{e}=', num2str(ke), '\:[Nm/rad]$']);
        set(label, 'Interpreter', 'Latex', 'FontSize', 10);
        i = i + 1;
    end
    hold off
    grid on
    
    tit = title('$ADAPTIVE\:\:|\:\:J_e=2.1e-3$');
    set(tit, 'Interpreter', 'Latex');

    ystart = -0.1;
    yend = 0.1;
    s2 = subplot(4, 1, 2);
    plot(s2, t, xr1 - tau, 'r', 'LineWidth', 2)
    l2 = legend('$e\:[Nm]$');
    set(l2, 'Interpreter', 'Latex');
    hold on
    i = 1;
    for idx = 0 : step : endTime - step
        ke = ke_vect(i);
        plot([idx, idx], [ystart, yend], 'b', 'Linewidth', 1.2);
        label = text(idx + 0.58, -0.14, ['$k_{e}=', num2str(ke), '\:[Nm/rad]$']);
        set(label, 'Interpreter', 'Latex', 'FontSize', 10);
        i = i + 1;
    end
    hold off
    grid on
    
    ystart = 0;
    yend = 1;
    s3 = subplot(4, 1, 3);
    plot(s3, t, c, 'k', 'LineWidth', 2)
    l3 = legend('$\hat{c}$');
    set(l3, 'Interpreter', 'Latex');
    hold on
    i = 1;
    for idx = 0 : step : endTime - step
        ke = ke_vect(i);
        plot([idx, idx], [ystart, yend], 'b', 'Linewidth', 1.2);
        label = text(idx + 0.58, -0.19, ['$k_{e}=', num2str(ke), '\:[Nm/rad]$']);
        set(label, 'Interpreter', 'Latex', 'FontSize', 10);
        i = i + 1;
    end
    hold off
    grid on

    ystart = 0;
    yend = 0.6;
    s4 = subplot(4, 1, 4);
    plot(s4, t, b, 'k', 'LineWidth', 2)
    l4 = legend('$\hat{b}$');
    set(l4, 'Interpreter', 'Latex');
    hold on
    for idx = 0 : step : endTime - step
        plot([idx, idx], [ystart, yend], 'b', 'Linewidth', 1.2);
    end
    hold off
    grid on

    xl = xlabel('$t\:[s]$');
    set(xl, 'Interpreter', 'Latex');

    linkaxes([s1, s2, s3, s4], 'x');

    fclose(fid);
end