clc;
clear;
clear global;
close all;


%%
filepath = '../../Log/';
FileList = dir([filepath, 'sliding*.csv']);
N = size(FileList, 1);

for k = 1 : N
    filename = FileList(k).name;
    disp(filename);

    fid = fopen([filepath, filename]);
    DATA = textscan(fid, '%f %f %f %f %f %f %f %f %f');

    D = cell2struct(DATA(1, 1), 'A', 1);
    t = struct2array(D);

    D = cell2struct(DATA(1, 2), 'A', 1);
    err = struct2array(D);

    D = cell2struct(DATA(1, 3), 'A', 1);
    derr = struct2array(D);

    D = cell2struct(DATA(1, 4), 'A', 1);
    ref = struct2array(D);

    D = cell2struct(DATA(1, 5), 'A', 1);
    dref = struct2array(D);

    D = cell2struct(DATA(1, 6), 'A', 1);
    phi = struct2array(D);

    D = cell2struct(DATA(1, 7), 'A', 1);
    eta = struct2array(D);

    D = cell2struct(DATA(1, 8), 'A', 1);
    s = struct2array(D);

    D = cell2struct(DATA(1, 9), 'A', 1);
    out = struct2array(D);


    startTime = 0;
    endTime = 25;
    step = 5;
    ke_vect = [0.1, 0.2, 0.4, 0.8, 1.6];

    f = figure('Position', [10, 50, 600, 400] * 1.5);
    set(f, 'PaperPositionMode', 'auto')
    
    s1 = subplot(2, 1, 1);
    plot(s1, t, err + ref, 'b', t, ref, 'r--')
    l1 = legend('$\tau_s\:[Nm]$', '$\tau_d\:[Nm]$');
    axis([startTime, endTime, 0.3, 0.85])
    set(l1, 'Interpreter', 'Latex');

    ystart = -0.05;
    yend = 0.1;
    hold on
    i = 1;
    for idx = 0 : step : endTime - step
        ke = ke_vect(i);
        plot([idx, idx], [ystart, yend], 'k', 'Linewidth', 1.2);
        label = text(idx + 0.6, -0.08, ['$k_{e}=', num2str(ke), '\:[Nm/rad]$']);
        set(label, 'Interpreter', 'Latex', 'FontSize', 10);
        i = i + 1;
    end
    hold off
    grid on
    
    tit = title('$SLIDING\:MODE\:\:|\:\:J_e=2.1e-3$');
    set(tit, 'Interpreter', 'Latex');

    ystart = -0.1;
    yend = 0.15;
    s2 = subplot(2, 1, 2);
    plot(s2, t, err, 'r', 'LineWidth', 2)
    l2 = legend('$e\:[Nm]$');
    set(l2, 'Interpreter', 'Latex');
    hold on
    for idx = 0 : step : endTime - step
        plot([idx, idx], [ystart, yend], 'b', 'Linewidth', 1.2);
    end
    hold off
    grid on

    linkaxes([s1, s2], 'x');

    figure(2)
    plot(t, s, t, 10 * eta, 'r', t, phi, 'g', t, -phi, 'g')
    grid on
    legend('s', '10 * eta', 'phi')

    fclose(fid);

    plen = 3000 / 4;
    len = endTime * 3000 - 1;
    % err = tau - ref;
end