clc;
clear;
clear global;
close all;


%%
filepath = '../../Log/';
FileList = dir([filepath, 'DOB*.csv']);
N = size(FileList, 1);

for k = 1 : N
    filename = FileList(k).name;
    disp(filename);

    fid = fopen([filepath, filename]);
    DATA = textscan(fid, '%f %f %f %f %f %f %f %f %f');

    D = cell2struct(DATA(1, 1), 'A', 1);
    ref = struct2array(D);

    D = cell2struct(DATA(1, 2), 'A', 1);
    dref = struct2array(D);

    D = cell2struct(DATA(1, 3), 'A', 1);
    tau = struct2array(D);

    D = cell2struct(DATA(1, 4), 'A', 1);
    dtau = struct2array(D);

    D = cell2struct(DATA(1, 5), 'A', 1);
    d_hat = struct2array(D);

    D = cell2struct(DATA(1, 6), 'A', 1);
    kp = struct2array(D);

    D = cell2struct(DATA(1, 7), 'A',1);
    ref = struct2array(D);

    D = cell2struct(DATA(1, 8), 'A', 1);
    ref = struct2array(D);

    D = cell2struct(DATA(1, 9), 'A', 1);
    t = struct2array(D);
    
    
    startTime = 0;
    endTime = 25;
    step = 5;
    ke_vect = [0.1, 0.2, 0.4, 0.8, 1.6];
    
    f = figure('Position', [10, 50, 600, 400] * 1.5);
    set(f, 'PaperPositionMode', 'auto')

    s1 = subplot(3, 1, 1);
    plot(s1, t, tau, 'b', t, ref, 'r--')
    l1 = legend('$\tau_s\:[Nm]$', '$\tau_d\:[Nm]$');
    axis([startTime, endTime, -0.2, 1.5])
    set(l1, 'Interpreter', 'Latex');
    
    ystart = -0.05;
    yend = 0.1;
    hold on
    i = 1;
    for idx = 0 : step : endTime - step
        ke = ke_vect(i);
        plot([idx, idx], [ystart, yend], 'k', 'Linewidth', 1.2);
        label = text(idx + 0.58, -0.08, ['$k_{e}=', num2str(ke), '\:[Nm/rad]$']);
        set(label, 'Interpreter', 'Latex', 'FontSize', 10);
        i = i + 1;
    end
    hold off
    grid on
    
    OL_2 = strcmp(filename, 'DOBControl_OL.csv');
    CL_2 = strcmp(filename, 'DOBControl_CL.csv');
    OL_3 = strcmp(filename, 'DOBControl_OL_3_order.csv');
    CL_3 = strcmp(filename, 'DOBControl_CL_3_order.csv');

    if (CL_2 == 1)
    	tit = title('$DOB\:\:|\:\:Closed-Loop\:-\:2nd\:Order\:Plant\:\:|\:\:J_e=2.1e-3$');
    elseif (CL_3 == 1)
    	tit = title('$DOB\:\:|\:\:Closed-Loop\:-\:3rd\:Order\:Plant\:\:|\:\:J_e=2.1e-3$');
    elseif (OL_2 == 1)
    	tit = title('$DOB\:\:|\:\:Open-Loop\:-\:2nd\:Order\:Plant\:\:|\:\:J_e=2.1e-3$');
    else
    	tit = title('$DOB\:\:|\:\:Open-Loop\:-\:3rd\:Order\:Plant\:\:|\:\:J_e=2.1e-3$');
    end
    set(tit, 'Interpreter', 'Latex');

    ystart = -0.4;
    yend = 0.4;
    s2 = subplot(3, 1, 2);
    plot(s2, t, ref - tau, 'r', 'LineWidth', 2)
    l2 = legend('$e\:[Nm]$');
    set(l2, 'Interpreter', 'Latex');
    hold on
    i = 1;
    for idx = 0 : step : endTime - step
        ke = ke_vect(i);
        plot([idx, idx], [ystart, yend], 'b', 'Linewidth', 1.2);
        label = text(idx + 0.58, -0.58, ['$k_{e}=', num2str(ke), '\:[Nm/rad]$']);
        set(label, 'Interpreter', 'Latex', 'FontSize', 10);
        i = i + 1;
    end
    hold off
    grid on

    if (CL_2 == 1)
        ystart = 0.2;
        yend = -0.2;
    elseif (CL_3 == 1)
        ystart = 0.1;
        yend = -0.1;
    elseif (OL_2 == 1)
        ystart = 1;
        yend = -1;
    else
        ystart = 15;
        yend = -5;
    end

    s3 = subplot(3, 1, 3);
    plot(s3, t, d_hat, 'k', 'LineWidth', 2)
    l3 = legend('$\hat{d}$');
    set(l3, 'Interpreter', 'Latex');
    hold on
    for idx = 0 : step : endTime - step
        plot([idx, idx], [ystart, yend], 'b', 'Linewidth', 1.2);
    end
    hold off
    grid on

    xl = xlabel('$t\:[s]$');
    set(xl, 'Interpreter', 'Latex');

    linkaxes([s1, s2, s3], 'x');

    fclose(fid);
end