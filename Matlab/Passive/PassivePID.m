clear
% close all
filepath = '../../SeaControl/';
files = dir([filepath 'PassiveV*.csv']);
% 
% filepath = 'sin/';
% files = dir([filepath 'PassivePIDLow.csv']);
% files = dir([filepath 'PassivePIDHigh.csv']);
% files = dir([filepath 'PassivePID1.csv']);

filelist = files;

startTime = 0;
endTime =3.5;
% endTime = 30;

for i=1:length(filelist)
filelist(i).name

fid = fopen([filepath filelist(i).name]); 
DATA = textscan(fid,'%f %f %f %f %f %f %f %f %f %f %f');

D = cell2struct(DATA(1,1),'A',1);
ref = struct2array(D);

D = cell2struct(DATA(1,2),'A',1);
dref = struct2array(D);

D = cell2struct(DATA(1,3),'A',1);
err = struct2array(D);

D = cell2struct(DATA(1,4),'A',1);
derr = struct2array(D);


D = cell2struct(DATA(1,5),'A',1);
t = struct2array(D);

n = 2;

endTime = t(end);

tau = err+ref;
dtau = derr+dref;

% to account for 20Hz
ref = 0.9393*ref;
err = tau-ref;

f = figure('Position', [0, 0, 800, 150*n]*1.2);
set(f,'PaperPositionMode','auto')

s1 = subplot(n,1,1)
plot(t,ref,'--k',t,tau,'k')%,'LineWidth',2.0)
l1= legend('$\tau_r$','$\tau_s$');
yl1 = ylabel('$torque  [Nm]$');
% axis([startTime endTime -0.5 0.5 ])
set(l1,'Interpreter','Latex');
set(yl1,'Interpreter','Latex');


s2 = subplot(n,1,2)
plot(t,err,'k')
yl2 = ylabel('$torque [Nm]$');
l2 = legend('$e$');
% axis([startTime endTime -0.2 0.2])
set(l2,'Interpreter','Latex');
set(yl2,'Interpreter','Latex');

% xl = xlabel('$t [s]$');
% set(xl,'Interpreter','Latex');
% linkaxes([s1 s2],'x');


print(f,'-painters','-depsc',strrep(filelist(i).name,'csv','eps'))

fclose(fid);

plen = 3000/4;
len = endTime*3000-1;
[rms s_rms max_value s_max_value] = errorPeriodAnalysis(err(2:end),len,plen)

end



