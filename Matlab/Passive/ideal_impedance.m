clear
Jl = 0.00025;
d = 0.005 + 0.002;
k = 0.8;

f = sqrt(k/Jl)/(2*pi)

S = tf(1,[Jl d k])
[tau, t] = impulse(S,1);

figure(10)
plot(t,-0.008*tau)