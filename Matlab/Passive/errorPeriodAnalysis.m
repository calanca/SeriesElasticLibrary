function [rms s_rms max_value s_max_value] = errorPeriodAnalysis(err,len,plen)

rms_w = zeros(1,floor(len/plen));
max_value_w = zeros(1,floor(len/plen));

% figure(100)
j = 1;
for i = 1:plen:len-plen
	
	err_w = err(i:i+plen-1);
	m = mean(err_w);
	err_w = err_w -m;
	rms_w(j) = sqrt(mean(err_w.^2));
	max_value_w(j) = max(abs(err_w));
% 	plot(err_w)
% 	pause(0.2)
	j = j + 1;
	
end


% ----- outliers ----------

s_rms = std(rms_w);
s_max_value = std(max_value_w);

m_rms = mean(rms_w);
m_max_value = mean(max_value_w);

rms_w(rms_w-m_rms > 3*s_rms)=NaN;
max_value_w(max_value_w-m_max_value > 3*s_max_value)=NaN;

% -------------------------

rms = nanmean(rms_w);
max_value = nanmean(max_value_w);

s_rms = nanstd(rms_w);
s_max_value = nanstd(max_value_w);
