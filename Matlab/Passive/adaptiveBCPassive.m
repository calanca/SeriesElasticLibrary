clear
% close all
filepath = '../../SeaControl/';
files = dir([filepath 'adaptive.csv']);


filelist = files;



startTime = 0;
endTime = 3;

for i=1:length(filelist)
filelist(i).name

fid = fopen([filepath filelist(i).name]); 
DATA = textscan(fid,'%f %f %f %f %f %f %f %f %f %f %f');

D = cell2struct(DATA(1,1),'A',1);
ref = struct2array(D);

D = cell2struct(DATA(1,2),'A',1);
xr1 = struct2array(D);

D = cell2struct(DATA(1,3),'A',1);
xr2 = struct2array(D);

D = cell2struct(DATA(1,4),'A',1);
x_tilde= struct2array(D);

D = cell2struct(DATA(1,5),'A',1);
dx_tilde = struct2array(D);

D = cell2struct(DATA(1,6),'A',1);
b_est = struct2array(D);

D = cell2struct(DATA(1,7),'A',1);
c_est = struct2array(D);

D = cell2struct(DATA(1,9),'A',1);
dc_est = struct2array(D);

D = cell2struct(DATA(1,10),'A',1);
h0_est = struct2array(D);
L = h0_est(1);
% D = cell2struct(DATA(1,9),'A',1);
% d_est = struct2array(D);
% 
% D = cell2struct(DATA(1,10),'A',1);
% eo_est = struct2array(D);

D = cell2struct(DATA(1,11),'A',1);
t = struct2array(D);

n = 5;
% startTime = 1;
% endTime = 5;

endTime = t(end);

tau = x_tilde+xr1;
dtau = dx_tilde+ xr2;


f = figure('Position', [0, 0, 800, 150*n]);
set(f,'PaperPositionMode','auto')

s1 = subplot(n,1,1)
plot(t,xr1,'--k',t,tau,'k')%,'LineWidth',2.0)
l1= legend('$\tau_r$','$\tau_h$');
yl1 = ylabel('$torque  [Nm]$');
axis([startTime endTime -1.5 1.5 ])
set(l1,'Interpreter','Latex');
set(yl1,'Interpreter','Latex');


s2 = subplot(n,1,2)
plot(t,x_tilde,'k')
% plot(t,dc_est,'k')
yl2 = ylabel('$torque [Nm]$');
l2 = legend('$e$');
% axis([startTime endTime -0.2 0.2])
set(l2,'Interpreter','Latex');
set(yl2,'Interpreter','Latex');

% 
s3 = subplot(n,1,3)
plot(t,c_est,'k')
l3= legend('$\hat{c}$');
% axis([startTime endTime 0 4])
set(l3,'Interpreter','Latex');
% 
s4 = subplot(n,1,4)
plot(t,b_est,'k')
l4 = legend('$\hat{b}$');
yl4 = ylabel('$[s]$');
% axis([startTime endTime 0 0.02])
set(l4,'Interpreter','Latex');
set(yl4,'Interpreter','Latex');

A = 0.00039;

kp = A*L^2-c_est;
dkp = -dc_est;
Edkp = 0.5*dkp.*tau.^2;

kd = 2*A*L - b_est;
Ekd = kd.*dtau.^2; 

s5 = subplot(n,1,5)
% plot(t,Edkp,'k')
plot(t,Edkp,'k',t,Ekd,'--k')
l5 = legend('$En(\dot Kp)$','$E_{dis}$');
yl5 = ylabel('$[J]$');
% axis([startTime endTime 0 0.02])
set(l5,'Interpreter','Latex');
set(yl5,'Interpreter','Latex');



xl = xlabel('$t [s]$');
set(xl,'Interpreter','Latex');

linkaxes([s1 s2 s3 s4 s5],'x');

f1 = figure
plot(t,Edkp,'k',t,Ekd,'--k')
l = legend('$En(\dot Kp)$','$E_{dis}$');
yl = ylabel('$[J]$');
% axis([startTime endTime 0 0.02])
set(l,'Interpreter','Latex');
set(yl,'Interpreter','Latex');

% print(1,'-painters','-depsc',strrep(filelist(i).name,'csv','eps'))

fclose(fid);


plen = 3000/4;
len = endTime*3000-1;
% err = tau-ref;
err = x_tilde;
[rms s_rms max_value s_max_value] = errorPeriodAnalysis(err(2:end),len,plen)

end



