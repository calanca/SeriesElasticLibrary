% Examples 
close all
clear
f = figure('Position', [200, 200, 350, 600]);
set(f,'PaperPositionMode','auto')


title = 'RMSad';
y = [27.3 	102.6 	9.3 	5.8  ; 69.0 	15.1 	13.9 	6.2]*0.001;
s = [0.28 	1.0 	0.99 	0.30 ; 0.47 	1.5 	1.2 	0.54 ]*0.001;

% title = 'MAXad';
% y = [ 41.4 	146.5	17.2 	12.1 ;99.6 	26.3 	18.9 	13.9]*0.001;
% s = [ 0.67 	1.1 	2.6 	1.1  ; 0.84 	2.9 	3.6 	1.3]*0.001;



% b = bar(y'); 
c=colormap(summer(4))
errorb(y',3*s','colormap',c);
errorb(y',3*s');
brighten(0.3)
hold off;
% t = title('RMS $\tau_r$','Fontsize',12)
% set(t,'Interpreter','Latex');

set(gca,'xtick',[1 2 3 4])
set(gca,'xticklabel',{'PID','PD','c','bc'})
%xticklabel_rotate([],45,[],'Fontsize',10)
set(gca,'Fontsize',11);
axis([0.5 4.5 0 max(max(y))+max(max(3*s)) ])
% axis([0.5 4.5 0 0.05 ])
% xlim([0.5 3.5])
legend('low','high')

print(f,'-painters','-depsc',[title '.eps'])
