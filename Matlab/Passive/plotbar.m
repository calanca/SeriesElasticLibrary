% Examples 
close all
clear
f = figure('Position', [200, 200, 300, 200]);
set(f,'PaperPositionMode','auto')


title = 'RMStr';
y = [6.7	12.8	5.8   ; 47.9	25.7	6.2 ]*0.001;
s = [ 0.7	0.3	    0.30 ;  0.2	1.2	  0.54 ]*0.001;

% title = 'MAXtr';
% y = [11.9	17.7	12.1 ; 69.2	38.9	13.9  ]*0.001;
% s = [ 0.8	0.7     1.1 ; 1.2	2.2	1.3]*0.001;

% title = 'RMStd';
% y = [30.5	20.1	6.6;	72.0	22.1	6.9 ]*0.001;
% s = [0.8	2.1 	0.4;	0.2	0.5	0.5 ]*0.001;

% title = 'MAXtd';
% y = [45.9	40.8	14.0;	103.2	35.8	14.6 ]*0.001;
% s = [1.0	5.0     1.1;	1.1	2.0	2.2 ]*0.001;



% b = bar(y'); 
c=colormap(summer(4))
errorb(y',3*s','colormap',c);
errorb(y',3*s');
brighten(0.3)
hold off;
% t = title('RMS $\tau_r$','Fontsize',12)
% set(t,'Interpreter','Latex');

set(gca,'xtick',[1 2 3])
set(gca,'xticklabel',{'P','PAB','HA'})
%xticklabel_rotate([],45,[],'Fontsize',10)
set(gca,'Fontsize',11);
axis([0.5 3.5 0 max(max(y))+max(max(3*s))])
% xlim([0.5 3.5])
legend('high','low')

print(f,'-painters','-depsc',[title '1.eps'])
