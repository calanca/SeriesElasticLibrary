% Examples 
close all
clear
f = figure('Position', [200, 200, 350, 600]);
set(f,'PaperPositionMode','auto')


title = 'RMSsm';
y = [69.0 	15.1 	13.9	5.3     6.0 ; 27.3      102.6 	21.0	9.5     6.2]*0.001;
s = [0.47 	1.5 	0.3     0.4     0.19 ; 0.28 	1.0 	0.3     0.8     0.27 ]*0.001;

% title = 'MAXsm';
% y = [99.6 	26.3 	22.4	10.8	13.6 ;41.4 	146.5 	32.0	18.6	13.5]*0.001;
% s = [ 0.84 	2.9 	1.2 	1.0     1.0 ; 0.67 	1.1 	1.0     2.4     2.1]*0.001;



% b = bar(y'); 
c=colormap(summer(4))
errorb(y',3*s','colormap',c);
errorb(y',3*s');
brighten(0.3)
hold off;
% t = title('RMS $\tau_r$','Fontsize',12)
% set(t,'Interpreter','Latex');

set(gca,'xtick',[1 2 3 4 5])
set(gca,'xticklabel',{'PID','PD','ILA','ILAR','AD'})
%xticklabel_rotate([],45,[],'Fontsize',10)
set(gca,'Fontsize',11);
axis([0.5 5.5 0 max(max(y))+max(max(3*s)) ])
% axis([0.5 4.5 0 0.05 ])
% xlim([0.5 3.5])
legend('high','low')

print(f,'-painters','-depsc',[title '.eps'])
