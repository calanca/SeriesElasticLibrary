clear

cutoff = 20; %hz
w = 2*pi*cutoff;

fder = tf([w 0],[1 w])
fder_d = c2d(fder,1/10000, 'tustin')

% fder = tf([1 0],[])
% fder_d = c2d(fder,1/10000, 'tustin')

