cutoff = 50; %hz
w = 2*pi*cutoff;
pole1 = tf(w,[1 w])

mr = pole1*pole1;

Ts = 10000;

implicit = c2d(pole1,1/Ts, 'tustin')
explicit = tf([0.09942 0],[1, -0.9006],1/Ts)

disp('semi-implicit')
semi_implicit = explicit*implicit

figure(54)
bode(semi_implicit,explicit*explicit,mr)
legend('semi-implicit','explicit','continuous')

