clear
close all
% filepath = 'Data\'
filepath = '../../../c++/tele/';
fid = fopen([filepath 'df.csv']); 
DATA = textscan(fid,'%f %f %f %f %f','delimiter',',')


D = cell2struct(DATA(1,1),'A',1);
TimeSec = struct2array(D);

D = cell2struct(DATA(1,2),'A',1);
a = struct2array(D);

D = cell2struct(DATA(1,3),'A',1);
b = struct2array(D);


figure(2)
plot(TimeSec,a,TimeSec,b)

fclose(fid);
