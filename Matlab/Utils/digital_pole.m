cutoff = 2; %hz
Fs = 1000;
w = 2*pi*cutoff;
pole1 = tf(w,[1 w])

right = c2d(pole1,1/Fs, 'zoh')
wrong = tf([0.09942 0],[1, -0.9006],1/3000)
figure(54)
bode(wrong,right,pole1)
legend('explicit','implicit','continuous')

