clear
cutoff = 10; %hz
Fs = 600;
w = 2*pi*cutoff;
pole1 = tf(w,[1 w])
right = c2d(pole1,1/Fs, 'matched')
