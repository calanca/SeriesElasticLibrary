% format long

Ts = 0.0003333;

freq = 200; %hz
w = 2*pi*freq;

lc = w*w;
lb = 2*w;

lambda = tf([1 lb lc],1);
s = tf([1 0],1);

w2 = 1/lambda

w2_dis1 = c2d(w2,Ts)
w2_dis2 = c2d(w2,Ts,'method','tustin')

figure(1)
bode(w2)
hold on
pause
bode(w2_dis1)
pause
bode(w2_dis2)
%ss(w2_dis)
hold off