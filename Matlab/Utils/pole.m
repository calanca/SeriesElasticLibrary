cutoff = 5; %hz
Fs = 3000;
w = 2*pi*cutoff;
pole1 = tf(w,[1 w])

right = c2d(pole1,1/Fs, 'zoh')

a = exp(-2*pi*cutoff/Fs)

ste = tf([(1-a) 0],[1, -a],1/Fs)

figure(54)
bode(ste,right,pole1)
legend('stefano','discrete','continuous')