% in Hardware.cpp decommentare  //dthetaE = jerk_envelopeM;//for jerk debug

clear
close all
matpath = 'Data\';
filepath = '../../../SeaControl/';
%filepath = 'Csv\'

files = dir([filepath 'jerk*.csv']);
filelist = files;

N = 6;%time + 5 var
%N = 7;%time + 6 var

for i=1:length(filelist)
filelist(i).name
fid = fopen([filepath filelist(i).name]); 
DATA = textscan(fid,'%f %f %f %f %f %f %f %f %f');

D = cell2struct(DATA(1,1),'A',1);
TimeSec = struct2array(D);

D = cell2struct(DATA(1,2),'A',1);
ActiveCurrentA = struct2array(D);

D = cell2struct(DATA(1,3),'A',1);
CurrentCommand = struct2array(D);

D = cell2struct(DATA(1,4),'A',1);
Position = struct2array(D);

D = cell2struct(DATA(1,5),'A',1);
Velocity = struct2array(D);

D = cell2struct(DATA(1,6),'A',1);
pVelocity = struct2array(D);

D = cell2struct(DATA(1,7),'A',1);
AuxiliaryPosition = struct2array(D);

D = cell2struct(DATA(1,8),'A',1);
AuxiliaryVelocity = struct2array(D);

D = cell2struct(DATA(1,9),'A',1);
pAuxiliaryVelocity = struct2array(D);


figure(1)
subplot(N,1,1)
plot(TimeSec,ActiveCurrentA)
subplot(N,1,2)
plot(TimeSec,CurrentCommand)
subplot(N,1,3)
plot(TimeSec,Position)
subplot(N,1,4)
plot(TimeSec,Velocity,TimeSec,pVelocity)
subplot(N,1,5)
plot(TimeSec,AuxiliaryPosition)
subplot(N,1,6)
plot(TimeSec,AuxiliaryVelocity,TimeSec,pAuxiliaryVelocity)

realEnvelope = pAuxiliaryVelocity;

pJerk = diff(pVelocity);
Jerk = diff(Velocity);
[B,A] = butter(1,0.001);
B = [0.002092 0.0]; 
A = [1.0,-0.9979];

envelope = filter(B,A,abs(Jerk));
idx = find(abs(pJerk) > 10*envelope);
figure(3)
% plot(TimeSec(2:end),Jerk,TimeSec(2:end),pJerk,TimeSec(2:end),10*envelope,TimeSec(2:end),-10*envelope,'r');
% legend('jerk','pjerk','env')
plot(TimeSec(2:end),Jerk,TimeSec(2:end),pJerk,TimeSec(2:end),10*envelope,TimeSec(2:end),-10*envelope,'r',TimeSec,10*realEnvelope,'c',TimeSec,10*realEnvelope,'c');
legend('jerk','pjerk','env','realEnv')


figure(2)
plot(TimeSec,Velocity,TimeSec,pVelocity,TimeSec(idx),pVelocity(idx),'r+');

% pause
% eval(['save ' matpath '' strrep(filelist(i).name,'csv','mat') ' ActiveCurrentA CurrentCommand Position Velocity pVelocity AuxiliaryPosition AuxiliaryVelocity pAuxiliaryVelocity TimeSec']);

fclose(fid);
end



