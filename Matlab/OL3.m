clc;
clear;
clear global;
close all;


%%
set(cstprefs.tbxprefs, 'FrequencyUnits', 'Hz')

nfig = 1;

s = tf([1, 0], 1);

% Motor Parameters
Jm = 0.02078;
dm = 0.0;

ke_vec = [0];
Je_vec = [0.00210];
xi_e = 1;

lenKe = length(ke_vec);
lenJe = length(Je_vec);


%% Plot Settings
figure(nfig);
col = jet(lenJe);

%% System
fq = 20;    % Hz
wq = 2 * pi * fq;
xiq = 1;
Q = wq ^ 2 / (s ^ 2 + 2 * 1 * wq * s + wq ^ 2);

% Controller
% kp = 15;
% kd = 2 * (kp + 1) / wq;
% C = kp + s * kd;

k_spring = 2.4882 %[10 100 1000]


for ke_index = 1 : lenKe
    for Je_index = 1 : lenJe
        ke = ke_vec(ke_index);
        Je = Je_vec(Je_index);

        de = 2 * xi_e * sqrt(ke * Je);

        N = Jm * Je * s ^ 3 + (Jm * de + Je * dm) * s ^ 2 + (dm * de + k_spring * (Jm + Je)) * s + k_spring * (dm + de);
        D = Je * s ^ 3 + (de + 2 * xiq * wq * Je) * s ^ 2 + wq * (wq * Je + 2 * xiq * de) * s + wq ^ 2 * de;

        P = (wq ^ 2 / k_spring) * N / D;
        stability = isstable(minreal(P))

        f = figure(nfig);
        bode(P, 'b')
        grid on

        xlim([1e-1, 1e3])

        tit = title(['$f_{q}=' num2str(fq) '\:Hz,\:J_{e}=' num2str(Je_vec(Je_index)) '\:Kg \cdot m^{2},\:k_{e}=' num2str(ke_vec(ke_index)) '\:Nm/rad,\:k=' num2str(k_spring) '\:Nm/rad\Longrightarrow r=' num2str(k_spring / ke) '$']);
        set(tit, 'Interpreter', 'Latex');

        set(f, 'Position', [20, 50, 600, 400] * 1.5)
        set(f, 'PaperPositionMode', 'auto')
        
        zpk(minreal(P))
        disp('static gain')
        dcgain(P)
        
        pause
    end
end
