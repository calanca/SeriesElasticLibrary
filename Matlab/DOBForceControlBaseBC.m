clc;
clear;
clear global;
% close all;


%%
filepath = '../Log/';
FileList = dir([filepath, 'DOB*.csv']);
N = size(FileList, 1);

for k = 1 : N
    filename = FileList(k).name;
    disp(filename);

    fid = fopen([filepath, filename]);
    DATA = textscan(fid, '%f %f %f %f %f %f %f %f %f');

    D = cell2struct(DATA(1, 1), 'A', 1);
    ref = struct2array(D);

    D = cell2struct(DATA(1, 2), 'A', 1);
    dref = struct2array(D);

    D = cell2struct(DATA(1, 3), 'A', 1);
    tau = struct2array(D);

    D = cell2struct(DATA(1, 4), 'A', 1);
    dtau = struct2array(D);

    D = cell2struct(DATA(1, 5), 'A', 1);
    d_hat = struct2array(D);

    D = cell2struct(DATA(1, 6), 'A', 1);
    kp = struct2array(D);

    D = cell2struct(DATA(1, 7), 'A',1);
    ref = struct2array(D);

    D = cell2struct(DATA(1, 8), 'A', 1);
    ref = struct2array(D);

    D = cell2struct(DATA(1, 9), 'A', 1);
    t = struct2array(D);

startTime = 0;
endTime = 25;%t(end);

n = 3;

f = figure('Position', [800, 800, 800, 150*n]*1.2);
set(f,'PaperPositionMode','auto')

s1 = subplot(n,1,1);
plot(t,ref,t,tau)%,'LineWidth',2.0)
l1= legend('$\tau_r[Nm]$','$\tau_h[Nm]$');
% yl1 = ylabel('$torque  [Nm]$');
axis([startTime endTime -0.0 1.5 ])
set(l1,'Interpreter','Latex');
% set(yl1,'Interpreter','Latex');

s2 = subplot(n,1,2);
plot(t,ref-tau)
% yl2 = ylabel('$torque [Nm]$');
l2 = legend('$e[Nm]$');
axis([startTime endTime -0.1 0.1])

set(l2,'Interpreter','Latex');
% set(yl2,'Interpreter','Latex');

% 
s3 = subplot(n,1,3);
plot(t,d_hat)
l3= legend('$\hat{d}$');
% axis([startTime endTime 0 1])
set(l3,'Interpreter','Latex');

xl = xlabel('$t[s]$');
set(xl,'Interpreter','Latex');

linkaxes([s1 s2 s3],'x');


fclose(fid);
pause
end

close all
%
% figure
% plot(t,ref,t,d_hat)
% legend('signal','filtered')

