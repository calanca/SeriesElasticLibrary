clear
filepath = '../Log/';

filelist = dir([filepath 'position*3Khz*.csv']);


for i=1:length(filelist)
filelist(i).name
fid = fopen([filepath filelist(i).name]); 
DATA = textscan(fid,'%f %f %f %f %f %f %f %f %f %f');

D = cell2struct(DATA(1,1),'A',1);
t = struct2array(D);

D = cell2struct(DATA(1,2),'A',1);
ActiveCurrent = struct2array(D);

D = cell2struct(DATA(1,3),'A',1);
CurrentCommand = struct2array(D);

D = cell2struct(DATA(1,4),'A',1);
theta_m = struct2array(D);

D = cell2struct(DATA(1,5),'A',1);
difftheta_m = struct2array(D);

D = cell2struct(DATA(1,6),'A',1);
dtheta_m  = struct2array(D);

D = cell2struct(DATA(1,7),'A',1);
theta_e = struct2array(D);

D = cell2struct(DATA(1,8),'A',1);
difftheta_e = struct2array(D);

D = cell2struct(DATA(1,9),'A',1);
dtheta_e = struct2array(D);

D = cell2struct(DATA(1,10),'A',1);
torque = struct2array(D);

N = 5;

end

f_cut = 20; % Hz
f_s = 3000; % Hz
[b, a] = butter(1,f_cut/(f_s/2));
dtheta_e_f = filter(b,a,dtheta_e);
difftheta_e_f = filter(b,a,difftheta_e);

figure()
plot(t,difftheta_e, t, dtheta_e,t,dtheta_e_f)
legend('diff','interperiod')
title('Velocity')

figure()
plot(t,difftheta_e_f, t, dtheta_e_f)
legend('diff','interperiod')
title('Velocity Filtered')

figure()
plot(t(2:end),diff(difftheta_e_f), t(2:end), diff(dtheta_e_f))
legend('diff','interperiod')
title('Acceleration Filtered')
