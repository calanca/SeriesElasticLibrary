#include <vector>
#include <algorithm>
#include <math.h>
#include <iostream>

#include "el3008.hpp"

#define MIN_VOLTAGE  0.0
#define MAX_VOLTAGE  10.0
#define MAX_DISCRETE 0x7FFF


EL3008::EL3008(int slaveID, ec_slavet * ec_slave) : ELXXXX(slaveID, ec_slave)
{
}

double EL3008::scale(short value)
{
    return ((double)value * MAX_VOLTAGE / (double)MAX_DISCRETE);
}

double EL3008::getAnalog(int channel)
{
    return scale(getRaw(channel));
}

short EL3008::getRaw(int channel)
{
    return (short)((unsigned short)(ec_slave[slaveID].inputs[4*channel-2]) + ((unsigned short)(ec_slave[slaveID].inputs[4*channel-1]) << 8));
}



