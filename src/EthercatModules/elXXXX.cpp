#include <iomanip>
#include <sstream>
#include "elXXXX.hpp"

ELXXXX::ELXXXX(int slaveID, ec_slavet * ec_slave)
{
    this->slaveID = slaveID;
    this->ec_slave = ec_slave;
}
string ELXXXX::getInputsAsString()
{
    string out;

    for (unsigned int i = 0; i < ec_slave[slaveID].Ibytes; i++)
    {        
        out += UIntToHexStr(ec_slave[slaveID].inputs[i]);
    }

    return out;
}

string ELXXXX::getOutputsAsString()
{
    string out;

    for (unsigned int i = 0; i < ec_slave[slaveID].Obytes; i++)
    {        
        out += UIntToHexStr(ec_slave[slaveID].outputs[i]);
    }

    return out;
}

string ELXXXX::UIntToHexStr(unsigned int tmp)
{
        std::ostringstream out;
        out << uppercase << setfill('0') << setw(2) << hex << tmp;
        return out.str();
}

