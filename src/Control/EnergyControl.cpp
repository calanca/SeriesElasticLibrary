#include "EnergyControl.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

EnergyControl::EnergyControl(ISEHardware* hw) : SEControl(hw)
{
    filter = DigitalFilter::getLowPassFilterHz(20);
	this->filename = "PEA-energy-";
	mu = 1 + JL/Jm;
	w = sqrt(Kspring*(1/Jm + 1/JL));
//	energy_ref = 0.1;
//	k_e = 0.1;

}

void EnergyControl::LogOpen()
{
	filenameStream << filename << "-en_" << energy_ref << "-k_e_" << k_e << ".csv";
	string s(filenameStream.str());
	logfile.open(s.c_str());
}

double EnergyControl::___process(double dt)
{

	energy = 0.5 * (Jm*pow(dtheta_m,2) + JL*pow(dtheta_e,2) + Kspring*pow(theta_m-theta_e,2));
	e = energy_ref - energy;
    kv = k_e*e;
	out = kv*(dtheta_m);
//	out = kv*sin(w*time);

	//friction compensation
	fcomp = frictionTorqueCompensationColoumb(dtheta_m);
//	fcomp = frictionTorqueCompensation(dtheta_m);

	fcomp += sign(dtheta_e)*0.012 * Kt;
//	fcomp += dtheta_e*0.0002 * Kt;

	cout << energy;
	return out + fcomp;
}

void EnergyControl::Log()
{

    logfile << time << " ";
    logfile << sehw->getThetaM() << " ";
    logfile << sehw->getDThetaM() << " ";
    logfile << sehw->getThetaE() << " ";
    logfile << sehw->getDThetaE() << " ";
    logfile << energy << " ";
    logfile << energy_ref << " ";
	logfile << sehw->getTauM() << " ";
    logfile << out << " ";
    logfile << kv << " ";
    logfile << endl;
}
