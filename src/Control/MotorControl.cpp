#include "MotorControl.hpp"

using namespace Eigen;

KEAdaptiveForceControl::KEAdaptiveForceControl(ISEHardware* hw) : SEControl(hw), ModelReference2(l1,l2)
{
	torqueFromDisplacement = false;
    G = 100;

	a_est = 0.1;
	ddtheta0_est = 0;

	pid = new PID(0,100,0);


//	this->filename = "Madaptive.csv";

	this->filename = "KEadaptive.csv";


	w0filt = DigitalFilter::getLowPassFilterHz(5);
	w1filt = DigitalFilter::getLowPassFilterHz(5);
    w2filt = DigitalFilter::getLowPassFilterHz(5);

	theta = Vector2d::Zero();
	theta(0) = 0.1;

	//gradient
	rho = Matrix2d::Zero();
	//LS
	P = Matrix2d::Identity(2,2);
	Q = Matrix2d::Identity(2,2);
	w = Vector2d::Zero();


	gradientUpdate = false;
	//speed
	//gradient
	rho(0,0) = G*0.01;
	rho(1,1) = G*0.01;
	//LS
	g = G*100;
	lambda = 0.999;
	ddtheta0Filter = DigitalFilter::getLowPassFilterHz(1);
}

double KEAdaptiveForceControl::___process(double dt)
{
    adaptationUpdate(dt);

//    out += frictionCurrentCompensation(xr2) * KT;

	pid->ref = this->ref;
	pid->dref = this->dref;
	pid->ddref = this->ddref;

	pid->KP = 10 / theta(0);

	out = ref + pid->process(tau,dtau,dt);

	return out;

}

void KEAdaptiveForceControl::adaptationUpdate(double dt)
{
	double ddtheta0, tau_motor;

	ddtheta0 = ddthetaMFilter->process(ddtheta_m);
	ddtheta0 = 0;
	tau_motor = hw->getTauM() - frictionTorqueCompensation(dtheta_m);

//	//system identification 1
//    y = w0filt->process( (tau - tau_motor)/Jm + ddtheta0  );
//    //Y = w0filt->process(out - A*ddtau);
//    w1 = w1filt->process(-1);
//    w2 = w2filt->process(ddtau);

	//system identification 2
    y = w0filt->process( tau  );
    w1 = w1filt->process( theta_m );
    w2 = w2filt->process( -1 );

    w << w1, w2;
    err = theta.dot(w) - y;

    if(gradientUpdate){
        //GRADIENT
        //dtheta = -rho*err*w;
        dtheta = -rho*err*w / (1+w.dot(w));
		theta = theta + dt*dtheta;

        		//trick!!!!!
		if(theta(0) < 0.2)
		{
			theta(0) = 0.2;
			theta(1) = theta(0) * theta_m - tau;
		}

        theta = theta + dt*dtheta;
    }
    else{
		//LS
        dtheta = -g*P*w*err;
        //dP = -g*P*w*w.transpose()*P;			//(1)
        //dP = Q - g*P*w*w.transpose()*P;		//(2)
        dP = -g*P*w*w.transpose()*P + lambda*P;	//(3)
		P = P + dt*dP;
//        P = 0.5 * (P + P.transpose());          //p+pT / 2
        theta = theta + dt*dtheta;

        //trick!!!!!
		if(theta(0) < 0.1)
		{
			theta(0) = 0.1;
			theta(1) = theta(0) * theta_m - tau;
		}


    }



//	cout << "ddtheta0 " << theta(0);
//    cout << "1/ke " << theta(1);
//    if(theta(1) < 0.0) theta(1) = 0.0;

//	if(theta(0) < 0.1) theta(0) = 0.1;
//    else if (theta(0) > 100.0) theta(0) = 100.0;

	log1 = theta(0);
	log2 = theta(1)/theta(0);
	log3 = P(0,0);
    log4 = P(1,1);

	cout << "ke " << theta(0);
    cout << " theta0 " << theta(1)/theta(0);
    cout << " KP " << pid->KP;




//	pid->KP = 1.0/theta(0);




//    b_est = theta(1);
//    c_est = theta(0);
////    da_est = - G * ( s*nu + 0*a_est );
//    a_est = a_est + dt*da_est;
//    cout << "a " << a_est;

//	if(b_est < 0.0) b_est = 0.0;
//	else if(b_est > A*2*L) b_est = A*2*L;
//   	log1 = hw->getThetaE();
//	log2 = hw->getThetaM();

}


KAdaptiveForceControl::KAdaptiveForceControl(ISEHardware* hw) : SEControl(hw)
{
	adaptationEnabled = 1;
	G = 100;

  	pid = new PID(0,40,0);

	this->filename = "Kadaptive.csv";


	w0filt = DigitalFilter::getLowPassFilterHz(5);
	w1filt = DigitalFilter::getLowPassFilterHz(5);
    w2filt = DigitalFilter::getLowPassFilterHz(5);

	theta = 0.1;

	//gradient
	rho = G;
	//LS
	P = 1;
	Q = 1;
	w = 1;


	gradientUpdate = false;
	g = G;
	lambda = 0.999;
	ddtheta0Filter = DigitalFilter::getLowPassFilterHz(1);
}


void KAdaptiveForceControl::Log()
{
    logfile << ref << " ";
    logfile << dref << " ";
    logfile << tau << " ";
    logfile << dtau << " ";
    logfile << theta_m << " ";
    logfile << dtheta_m << " ";
    logfile << theta_e << " ";
    logfile << dtheta_e << " ";
    logfile << torqueObserver << " ";
    logfile << log1 << " ";
    logfile << hw->getCurrent()*KT << " ";
    logfile << log3 << " ";
    logfile << this->time << " ";
    logfile << endl;

}

double KAdaptiveForceControl::___process(double dt)
{

    adaptationUpdate(dt);

//    out += frictionCurrentCompensation(xr2) * KT;

	pid->ref = this->ref;
	pid->dref = this->dref;
	pid->ddref = this->ddref;
//	out = pid->process(ctr->torqueObserver,ctr->diffTorqueObserver,dt) / KT;
	out = pid->process(tau,dtau,dt) / KT;
    return out;

}

void KAdaptiveForceControl::adaptationUpdate(double dt)
{
	double ddtheta0, tau_motor,new_theta;

	ddtheta0 = ddtheta_e;
	//ddthetaMFilter->process(ddtheta_m);

	tau_motor = hw->getTauM() - frictionTorqueCompensation(dtheta_m);

//	//system identification2
//    y = w0filt->process( (tau_motor - tau)/Jm - ddtheta_e  );
//    w = w1filt->process( ddtau );

	//system identification1
    y = w0filt->process( tau );
    w = w1filt->process( theta_m - theta_e );

    err = theta*w - y;

    if(gradientUpdate){
        //GRADIENT
        //dtheta = -rho*err*w;
        dtheta = -rho*err*w / (1+w*w);
        theta = theta + dt*dtheta;
    }
    else{
		//LS
        dtheta = -g*P*w*err;
        //dP = -g*P*w*w.transpose()*P;			//(1)
        //dP = Q - g*P*w*w.transpose()*P;		//(2)
        dP = -g*P*w*w*P + lambda*P;	//(3)
		P = P + dt*dP;
//        P = 0.5 * (P + P.transpose());          //p+pT / 2
        theta = theta + dt*dtheta;
    }

    if(theta < 1.0) theta = 1.0;

    pid->KP = 0/theta;

	log1 = theta;
    log2 = sehw->getCurrent()*KT;
    log3 = sehw->getThetaE();

	cout << "ke " << theta;
    cout << " P " << P;

}


DOBForceControl::DOBForceControl(ISEHardware* hw) : SEControl(hw)
{
    double Fdob, Fobs;
    Fdob = 10;
    Fobs = 10;
    Gdob = M_2_PI * Fdob;
    Gobs = M_2_PI * Fobs;
    dobFilter = DigitalFilter::getLowPassFilterHz(Fdob);
    obsFilter = DigitalFilter::getLowPassFilterHz(Fobs);

	this->torqueFromDisplacement = false;
	this->filename = "DOBControl.csv";

}

double DOBForceControl::___process(double dt)
{
    dob = dobFilter->process(KT*hw->getCurrent() + Gdob*Jm*dtheta_m) - Gdob*Jm*dtheta_m;

    obs = obsFilter->process(KT*hw->getCurrent() + Gobs*Jm*dtheta_m + frictionTorqueCompensation(dtheta_m)) - Gobs*Jm*dtheta_m;

    out = 0.98*dob - 10 * (obs - ref); // just the dob robustification
//    out = 0.7*dob - 0.3 * (tau - ref); // with the sensorless force feedback

	log1 = dob;
	log2 = obs;

//	out = 0;

	return out;
}

DOBForceControl::~DOBForceControl()
{
	delete dobFilter;
	delete obsFilter;
}

//
//KEAdaptiveForceControl::KEAdaptiveForceControl(double l1, double l2, ISEHardware* hw) : MotorControlBase(hw), ModelReference2(l1,l2)
//{
//	adaptationEnabled = 1;
//	referenceModelEnabled = true;
//
//    L = 2*M_PI*0.1;
//    G = 0.0000;
//
//    ni = 0;
//    phi = 5;
//
//	a_est = 0.1;
//	ddtheta0_est = 0;
//
//	this->filename = "Madaptive.csv";
//}
//
//
//void KEAdaptiveForceControl::Log()
//{
//    logfile << ref << " ";
//    logfile << tau << " ";
//    logfile << dtau << " ";
//    logfile << xr1 << " ";
//    logfile << xr2 << " ";
//    logfile << a_est << " ";
//    logfile << a_est << " ";
//    logfile << log1 << " ";
//    logfile << log2 << " ";
//    logfile << log3 << " ";
//    logfile << this->time << " ";
//    logfile << endl;
//
//}
//
//double KEAdaptiveForceControl::__process(double theta, double dtheta, double dt)
//{
//	//sliding parameters log
//    log1 = ni;
//	log2 = phi;
//	log3 = L;
//
//    if(referenceModelEnabled) MRUpdate(ref, dt);
//    else
//    {
//        xr1 = ref;
//        xr2 = dref;
//        f2 = ddref;
//    }
//
//    x_tilde	= tau - xr1;
//    dx_tilde = dtau - xr2;
//    nu = f2 - 0.1*L*dx_tilde - L*L*x_tilde;
//
//    s = dx_tilde + L*x_tilde;
////	s = L*x_tilde;
//
////    //sliding-mode robustification
////	us = sign(s);
////	if(fabs(s) < phi) us =  s / phi;
//
//    out = a_est * nu + tau - ni*us;
//
//    //adaptivity
//    if(adaptationEnabled) adaptationUpdate(dt);
//
////    out += frictionCurrentCompensation(xr2) * KT;
//    return out;
//
//}
//
//void KEAdaptiveForceControl::adaptationUpdate(double dt)
//{
//    da_est = - G * ( s*nu + 0*a_est );
//    a_est = a_est + dt*da_est;
//    cout << "a " << a_est;
//
////	if(b_est < 0.0) b_est = 0.0;
////	else if(b_est > A*2*L) b_est = A*2*L;
////   	log1 = hw->getThetaE();
////	log2 = hw->getThetaM();
//
//}
