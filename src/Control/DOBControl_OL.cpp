#include "DOBControl_OL.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Davide Ciocca, Andrea Calanca
 * @date Oct 2016
 ****************************************************************************/
DOBControlBase::DOBControlBase(double KP, double KD, double QcutoffHz, ISEHardware *hw) : SEControl(hw)
{
    this->KP = KP;
    this->KD = KD;
    this->QcutoffHz = QcutoffHz;

    this->FF = 1.0;

    this->w_q = 2 * M_PI * QcutoffHz;
    this->w_q2 = pow(w_q, 2);

    this->xi_q = 1;

    this->dm = 0.0;
    this->de = 0.0;

    this->name = "OL";

}

void DOBControlBase::LogOpen()
{

    int id = floor(drand()*100);
    filenameStream << LOG_DIRECTORY << "DOBControl" << id <<  "_" << name;
    if(dynamicFFEnabled) filenameStream << "_dynFF";
    filenameStream << "_KP" << KP <<"_FF" << FF << "_Q" << QcutoffHz << "Hz.csv";
    string s(filenameStream.str());
    logfile.open(s.c_str());
}

void DOBControlBase::Log()
{
    logfile << ref << " ";
    logfile << dref << " ";
    logfile << tau << " ";
    logfile << dtau << " ";
    logfile << d_hat << " ";
    logfile << KP << " ";
    logfile << FF << " ";
    logfile << ref << " ";
    logfile << this->time << " ";
    logfile << endl;
}

DOBControl_OL::DOBControl_OL(double KP, double KD, double QcutoffHz, ISEHardware *hw) : DOBControlBase(KP, KD, QcutoffHz, hw) {
    this->dynamicFFEnabled = false;

    // Q filter
    double temp_a_Q[3] = {1.0, 2 * xi_q * w_q, pow(w_q, 2)};
    this->a_Q = temp_a_Q;
    double temp_b_Q[3] = {0.0, 0.0, pow(w_q, 2)};
    this->b_Q = temp_b_Q;
    this->Q_filter = new AnalogFilter(2, a_Q, b_Q);

    // QPninvQ filter
    double temp_a_PninvQ[3] = {1 / pow(w_q, 2), 2 * xi_q / w_q, 1.0};
    this->a_PninvQ = temp_a_PninvQ;
    double temp_b_PninvQ[3] = {Jm / Kspring, dm / Kspring, 1.0};
    this->b_PninvQ = temp_b_PninvQ;

    this->PninvQ_filter = new AnalogFilter(2, a_PninvQ, b_PninvQ);
    this->PninvQ_filterFF = new AnalogFilter(2, a_PninvQ, b_PninvQ);

    this->Ud_prev = 0.0;
}

double DOBControl_OL::___process(double dt)
{
    double U;
    double Q_output, PninvQ_output, PninvQFF_output;

    // Error references
    err = ref - tau;
    derr = dref - dtau;

    // Filter ouput:
    PninvQFF_output = PninvQ_filterFF->process(ref, dt);

    // PD Controller
    if(dynamicFFEnabled) U = KP * err + KD * derr + PninvQFF_output;
    else U = KP * err + KD * derr + ref * FF;

    // Filter outputs:
    Q_output = Q_filter->process(Ud_prev, dt);
    PninvQ_output = PninvQ_filter->process(tau, dt);

    d_hat = PninvQ_output - Q_output;
    Ud = U - d_hat;
    Ud_prev = Ud;
    return Ud;
}


DOBControl_OL3order::DOBControl_OL3order(double KP, double KD, double QcutoffHz, ISEHardware *hw) : DOBControlBase(KP, KD, QcutoffHz, hw)
{
    this->FF = 0.0;

    this->Ud_prev = 0.0;


    // Q filter parameters
    double temp_a_Q[3] = {1.0, 2 * xi_q * w_q, w_q2};
    this->a_Q = temp_a_Q;
    double temp_b_Q[3] = {0.0, 0.0, w_q2};
    this->b_Q = temp_b_Q;
    this->Q_filter = new AnalogFilter(2, a_Q, b_Q);

    // PninvQ filter parameters
    double temp_a_PninvQ[4] = {JL, de + 2 * xi_q * w_q * JL, w_q * (2 * xi_q * de + w_q * JL), de * w_q2};
    this->a_PninvQ = temp_a_PninvQ;
    double temp_b_PninvQ[4] = {w_q2 * JL * Jm, w_q2 * (JL * dm + Jm * de), w_q2 * (de * dm + Kspring * (Jm + JL)), w_q2 * Kspring * (dm + de)};
    this->b_PninvQ = temp_b_PninvQ;

    this->PninvQ_filter = new AnalogFilter(2, a_PninvQ, b_PninvQ);
    this->PninvQ_FFfilter = new AnalogFilter(2, a_PninvQ, b_PninvQ);

    if(dynamicFFEnabled) this->filename = "DOBControl_OL3_dynFF";
    this->name = "OL3";

}

double DOBControl_OL3order::___process(double dt) {
    double U;
    double Q_output, PninvQ_output, FF_output;

    // Error references
    err = ref - tau;
    derr = dref - dtau;

    // Filter ouput:
    FF_output = PninvQ_FFfilter->process(ref, dt);

    // PD Controller
    if(dynamicFFEnabled) U = KP * err + KD * derr + FF_output;
    else U = KP * err + KD * derr + ref * FF;

    // Filter outputs:
    Q_output = Q_filter->process(Ud_prev, dt);
    PninvQ_output = PninvQ_filter->process(tau, dt);

    d_hat = PninvQ_output - Q_output;
    Ud = U - d_hat + 0.0 * Jm * ddtheta_e;
    Ud_prev = Ud;

    return Ud;
}

