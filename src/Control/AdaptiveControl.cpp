#include "AdaptiveControl.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/
//
using namespace Eigen;

ModelReference2::ModelReference2(double l1, double l2)
{
    this->l1 = l1;
    this->l2 = l2;
}

void ModelReference2::MRUpdate(double ref, double dt)
{
//	ref = Control::saturation(ref,1.0);

    //metodo simpletico (si invertono i calcoli di f1 e f2 e si arriva ad un semi implicito!!)
    f2 = -l1 * xr2 - l2 * xr1 + l2*ref;
    xr2 = prev_xr2 + dt * f2;

//	xr2 = max(xr2,15.0);

    f1 = xr2;
    xr1 = prev_xr1 + dt * f1;

    prev_xr1 = xr1;
    prev_xr2 = xr2;
}

AdaptiveForceControlBaseBC::AdaptiveForceControlBaseBC(double l1, double l2, ISEHardware* hw) : SEControl(hw), ModelReference2(l1,l2)
{
    adaptationEnabled = 1;
	referenceModelEnabled = true;

    L = 2*M_PI*10.0;
    G = 10;


    ni = 0;
    phi = 10;

    ka = 0;

	//estimated control parameters
	b_est = 0;
    c_est = 0;
    a_est = 0;

	//estimation of A
    thetaA = A;
	wAfilt = DigitalFilter::get10HzButtFilter();
	yAfilt = DigitalFilter::get10HzButtFilter();

	this->filename = "adaptive.csv";
}

void AdaptiveForceControlBaseBC::Log()
{
    logfile << ref << " ";
    logfile << tau << " ";
    logfile << dtau << " ";
    logfile << xr1 << " ";
    logfile << xr2 << " ";
    logfile << b_est << " ";
    logfile << c_est << " ";
    logfile << log1 << " ";
    logfile << log2 << " ";
    logfile << log3 << " ";
    logfile << this->time << " ";
    logfile << endl;

//    //legacy
//    logfile << ref << " ";
//    logfile << xr1 << " ";
//    logfile << xr2 << " ";
//    logfile << x_tilde << " ";
//    logfile << dx_tilde << " ";
//    logfile << b_est << " ";
//    logfile << c_est << " ";
//    logfile << log1 << " ";
//    logfile << log2 << " ";
//    logfile << log3 << " ";
//    logfile << this->time << " ";
//    logfile << endl;
}


double AdaptiveForceControlBaseBC::___process(double dt)
{
	//sliding parameters log
    log1 = ni;
	log2 = phi;
	log3 = L;

    if(referenceModelEnabled) MRUpdate(ref, dt);
    else
    {
        xr1 = ref;
        xr2 = dref;
        f2 = ddref;
    }

    x_tilde	= tau - xr1;
    dx_tilde = dtau - xr2;
    nu = f2 - 2.0*L*dx_tilde - L*L*x_tilde;

    s = dx_tilde + L*x_tilde;

    //sliding-mode robustification
	us = sign(s);
	if(fabs(s) < phi) us =  s / phi;

    out = ka*Jm*ddtheta_e +  A * nu + b_est*dtau + c_est*tau - ni*us;

    //adaptivity
    if(adaptationEnabled) adaptationUpdate(dt);

//    out += frictionCurrentCompensation(xr2) * Kt;

	    cout << "       " <<tau;
    return out;
}

void MRAdaptiveForceControlBC::adaptationUpdate(double dt)
{
   	log1 = 0;


    dc_est = - G*s*tau;
    c_est = c_est + dt*dc_est;
    cout << "c " << c_est;

	if(time > 0.2)
	{
	db_est = - G*s*dtau; // faulhaber motor
    b_est = b_est + dt*db_est;
    cout << "\t b " << b_est;
	}


	da_est = - 0.000001 * G * s* nu; // faulhaber motor
    a_est = a_est + dt*da_est;

//
	if(b_est < 0.0) b_est = 0.0;
	else if(b_est > A*2*L) b_est = A*2*L;

	if(c_est < 0.0) c_est = 0.0;
	else if(c_est > A*L*L) c_est = A*L*L;


	//estimation of A with gradient method
	yA = yAfilt->process(hw->getTauM());
//	yA = yAfilt->process(hw->getTauM()-frictionTorqueCompensation(dtheta_m)-b_est*dtau-c_est*tau);
    wA = wAfilt->process(-ddtau);
	errA = thetaA*wA - yA;
	dthetaA = -0.1*errA*wA / (1+wA*wA);
    thetaA = thetaA + dt*dthetaA;

	if(thetaA < 0.0) thetaA = 0.0;
    else if(thetaA > 0.01) thetaA = 0.01;

	log2 = a_est;
	log3 = thetaA;


//    a_est = thetaA; // enables the adaptation of A based on the indirect method (nor working properly) otherwise de direct method is used
	a_est = A;


	if(a_est< 0.0) a_est = 0.0;
    else if(a_est > 0.02) a_est = 0.02;

	cout << "\t a " << thetaA;

}

IndirectAdaptiveForceControlBC::IndirectAdaptiveForceControlBC(double l1, double l2, ISEHardware* hw) : AdaptiveForceControlBaseBC(l1,l2,hw)
{
	this->filename = "iadaptive.csv";
	gradientUpdate = false;

	w0filt = DigitalFilter::get10HzButtFilter();
	w1filt = DigitalFilter::get10HzButtFilter();
    w2filt = DigitalFilter::get10HzButtFilter();

	//gradient
	rho = Matrix2d::Zero();
	//LS
	theta = Vector2d::Zero();
	P = Matrix2d::Identity(2,2);
	Q = Matrix2d::Identity(2,2);
	w = Vector2d::Zero();

}

void IndirectAdaptiveForceControlBC::setAdaptationSpeed(double G)
{
	//gradient
	rho(0,0) = 10*G;
	rho(1,1) = 0.1*G;
	//LS
	g = 10*G;
	lambda = 0.999;
}

void IndirectAdaptiveForceControlBC::adaptationUpdate(double dt)
{
    //system identification
    y = w0filt->process(hw->getTauM() - A*ddtau);
    //Y = w0filt->process(out - A*ddtau);
    w1 = w1filt->process(tau);
    w2 = w2filt->process(dtau);

    w << w1, w2;
    err = theta.dot(w) - y;

    if(gradientUpdate){
        //GRADIENT
        //dtheta = -rho*err*w;
        dtheta = -rho*err*w / (1+w.dot(w));
    }
    else{
		//LS
        dtheta = -g*P*w*err;
        //dP = -g*P*w*w.transpose()*P;			//(1)
        //dP = Q - g*P*w*w.transpose()*P;		//(2)
        dP = -g*P*w*w.transpose()*P + lambda*P;	//(3)
        P = 0.5 * (P + P.transpose());          //p+pT / 2
        P = P + dt*dP;
    }

//    passivityConstraint();

    theta = theta + dt*dtheta;

    b_est = theta(1);
    c_est = theta(0);

    //for safety (<) and passivity (>)
    if(b_est < 0.0) b_est = 0.0;
    else if(b_est > A*2*L) b_est = A*2*L;
    if(c_est < 0.0) c_est = 0.0;
    else if(c_est > A*L*L) c_est = A*L*L;


    theta(1) = b_est;
    theta(0) = c_est;

	cout << "indirect: c " << c_est;
	cout << "b " << b_est;

	//estimation of A with gradient method
	yA = yAfilt->process(hw->getTauM());
    wA = wAfilt->process(-ddtau);
	errA = thetaA*wA - yA;
	dthetaA = -1*errA*wA / (1+wA*wA);
    thetaA = thetaA + dt*dthetaA;

	if(thetaA < 0.0) thetaA = 0.0;
    else if(thetaA > 1) thetaA = 1;

//	cout << "\t a " << a_est;
//	cout << "\t a " << thetaA;

    //log
    log1 = theta.dot(w); //model
    log2 = a_est;
    log3 = thetaA;

}

void IndirectAdaptiveForceControlBC::passivityConstraint()
{
    //passivity conditions
    kp = A*L*L - c_est;
    kd = 2*A*L - b_est;
    dkp = - dtheta(0);

    dkp_max = 2*kd*pow(dtau,2)/pow(tau,2);
    if(dkp > dkp_max)
    {
    	dkp = dkp_max;
    	cout << "passivity constrain ";
    }
    dtheta(0) = - dkp;
}

MultiAdaptiveForceControlBC::MultiAdaptiveForceControlBC(double l1, double l2, ISEHardware* hw): IndirectAdaptiveForceControlBC(l1,l2,hw)
{

    //modelFilt = DigitalFilter::getMeanFilter();
    modelFilt = DigitalFilter::getLowPassFilterHz(5);
    lambda = 0.5;
    g = 10;

    theta1 = Vector2d::Zero();
    theta2 = Vector2d::Zero();
    P1 = P;
    P2 = P;

    theta1(0) = 18;
    theta2(0) = 1;

};

void MultiAdaptiveForceControlBC::adaptationUpdate(double dt)
{
    //system identification
    y = w0filt->process(hw->getTauM() - A*ddtau);
    //Y = w0filt->process(out - A*ddtau);
    w1 = w1filt->process(tau);
    w2 = w2filt->process(dtau);

    w << w1, w2;
    err1 = theta1.dot(w) - y;
    err2 = theta2.dot(w) - y;

    switc = abs(err1) / ( abs(err1)+abs(err2) );

    switc= modelFilt->process(switc); //todo sostituire con isteresi

    if(switc<0.5)
    {
        err = err1;
        theta = theta1;
        P = P1;
    }
    else
    {
        err = err2;
        theta = theta2;
        P = P2;
    }

    //GRADIENT -> too fast for the MM
    //dtheta = -rho*err*w;
    //dtheta = -rho*err*w / (1+w.dot(w));

    //LS
    dtheta = -g*P*w*err;

    //dP = -g*P*w*w.transpose()*P;			//(1)
    //dP = Q - g*P*w*w.transpose()*P;		//(2)
    dP = -g*P*w*w.transpose()*P + lambda*P;	//(3) //discuti segni con riccardo


    //passivity conditions
//        kp = A*L*L - c_est;
//        kd = 2*A*L - b_est;
//        dkp = - dtheta(0);
//		dkp_max = 2*kd* dtau*dtau / tau*tau;
//
//		log3 = 0;
//        if(dkp > dkp_max) non è questo da controllare ma quando apsso d aun modello all'altro!!! - invecie nel caso non multi model è prorpio questo!! mi conviene tenere theta?
//							todo gerarchizzare questo e il padre.
//			{
//				log3 = dkp-dkp_max;
//				dkp = dkp_max;
//			}
//
//        if(theta(1) < 0.0) theta(1) = 0.0;
//        else if(theta(1) > A*2*L) theta(1) = A*2*L;
//
//        if(theta(0) < 0.0) theta(0) = 0.0;
//        else if(theta(0) > A*L*L) theta(0) = A*L*L;
//
//        dtheta(0) = - dkp;

    //updates
    theta = theta + dt*dtheta;
    P = P + dt*dP;
    P = 0.5 * (P + P.transpose()); //p+pT / 2


    if(switc<0.5)
    {
        theta1 = theta;
        P1 = P;
    }
    else
    {
        theta2 = theta;
        P2 = P;
    }

    cout << "\tC: " << theta(0);
    cout << "\tB: " << theta(1);


	log1 = theta.dot(w); //model
    log2 = y;
    log3 = switc;

    c_est = theta(0);
    b_est = theta(1);

}

MRPDControl::MRPDControl(double l1, double l2, ISEHardware* hw) : SEControl(hw), ModelReference2(l1,l2)
{
	iFilter = DigitalFilter::getLowPassFilterHz(1);
    this->filename = "MRPD.csv";
}

double MRPDControl::___process(double dt)
{
    //Control
    MRUpdate(ref, dt);

    err	= tau - xr1 ;
    derr = dtau - xr2;
    ierr = iFilter->process(err);//integral with roll-off
    //ierr += err*dt;

    out = - kp * err - kd * derr - ki * ierr;
	cout << "time: " << time;

    return out;
}


void MRPDControl::Log()
{
	logfile << ref << " ";
    logfile << xr1 << " ";
    logfile << xr2 << " ";
    logfile << err << " ";
    logfile << derr << " ";
    logfile << ref << " ";
    logfile << ref << " ";
    logfile << ref << " ";
    logfile << ref << " ";
    logfile << ref << " ";
    logfile << this->time << " ";
    logfile << endl;
}

