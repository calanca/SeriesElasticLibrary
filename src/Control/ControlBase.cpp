#include "ControlBase.hpp"


/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date july 2015
 ****************************************************************************/


MotorControlBase::MotorControlBase(IMotorHardware *hw)
{
	this->hw = hw;
	this->Jm = hw->Jm; // just for ease of use !!
	this->Kt = hw->Kt; // just for ease of use !!


    thetaMFilter = NULL;
    dthetaMFilter = DigitalFilter::getLowPassFilterHz(50);
    ddthetaMFilter = DigitalFilter::getLowPassFilterHz(50);
    jerkMDifferentiatorFilter = DigitalFilter::getLowPassDifferentiatorHz(50);

    torqueFilter = NULL;//DigitalFilter::getLowPassFilterHz(20);
    dtorqueFilter = NULL;//DigitalFilter::getLowPassFilterHz(20);
    ddtorqueDifferentiatorFilter = DigitalFilter::getLowPassDifferentiatorHz(10);
    torqueObserverFilter = DigitalFilter::getLowPassFilterHz(5);
    g = 31.4;
}

double MotorControlBase::_process(double theta, double dtheta, double dt)
{
	// get and filter motor data

	// motor position
	theta_m = hw->getThetaM();
	dtheta_m = hw->getDThetaM();
	ddtheta_m = hw->getDDiffThetaM();
    if(thetaMFilter != NULL) theta_m = thetaMFilter->process(hw->getThetaM());
    if(dthetaMFilter != NULL) dtheta_m = dthetaMFilter->process(hw->getDiffThetaM());
    if(ddthetaMFilter != NULL) ddtheta_m = ddthetaMFilter->process(hw->getDDiffThetaM());
    jerk_m = jerkMDifferentiatorFilter->process(hw->getDDiffThetaM());

	// motor torque (measured from sensor)
	tau = hw->getTorque();
	dtau = hw->getDiffTorque();
	if(torqueFilter != NULL) tau = torqueFilter->process( hw->getTorque() );
	if(dtorqueFilter != NULL) dtau = dtorqueFilter->process( hw->getDiffTorque() );

	ddtau = ddtorqueDifferentiatorFilter->process( hw->getDiffTorque() );

	// torque observer (virtual torque sensor)
	torqueObserver = torqueObserverFilter->process(Kt*hw->getCurrent() + g*Jm*dtheta_m - frictionTorqueCompensation(dtheta_m)) - g*Jm*dtheta_m;
	diffTorqueObserver = (torqueObserver - prev_torqueObserver) / dt;


	prev_torqueObserver = torqueObserver;
	return __process(theta, dtheta, dt);
}

SEControl::SEControl(ISEHardware *hw): MotorControlBase(hw)
{
	this->sehw = (ISEHardware*)hw;
	this->Kspring = hw->Kspring; // for ease of use !!
	this->A = Jm / Kspring; // for ease of use !!


	this->torqueFromDisplacement = true;
	thetaEFilter = NULL;
    dthetaEFilter = NULL; //DigitalFilter::getLowPassFilterHz(50);
    ddthetaEFilter = DigitalFilter::getLowPassFilterHz(20);
}

double SEControl::__process(double theta, double dtheta, double dt)
{
	theta_e = sehw->getThetaE();
	dtheta_e = sehw->getDThetaE();
	ddtheta_e = sehw->getDDiffThetaE();

    if(thetaEFilter != NULL) theta_e = thetaEFilter->process(sehw->getThetaE());
    if(dthetaEFilter != NULL) dtheta_e = dthetaEFilter->process(sehw->getDiffThetaE());
    if(ddthetaEFilter != NULL) ddtheta_e = ddthetaEFilter->process(sehw->getDDiffThetaE());

    // we may want to overwrite the torque measured from sensor with the torque measured from dispacement
    if(torqueFromDisplacement)
    {
		tau = Kspring * (theta_m - theta_e);
		dtau = Kspring * ( dtheta_m - dtheta_e );
		ddtau = Kspring * (ddtheta_m - ddtheta_e);
    }

	return ___process(dt);
}

void SEControl::Log()
{
    logfile << time << " ";
    logfile << hw->getCurrent() << " ";
    logfile << out << " ";
    logfile << theta_m << " ";
    logfile << dtheta_m << " ";
    logfile << ddtheta_m << " ";
    logfile << theta_e << " ";
    logfile << dtheta_e << " ";
    logfile << ddtheta_e << " ";
    logfile << tau << " ";
    logfile << dtau << " ";
    logfile << ddtau << " ";
    logfile << ref << " ";
    logfile << dref << " ";
    logfile << ddref << " ";
    logfile << endl;
}

SEControl::~SEControl()
{
    delete sehw;
	delete thetaEFilter;
	delete dthetaEFilter;
	delete ddthetaEFilter;
}

MotorControlBase::~MotorControlBase()
{
    delete hw;
	delete thetaMFilter;
	delete dthetaMFilter;
	delete ddthetaMFilter;
	delete torqueFilter;
	delete dtorqueFilter;
	delete ddtorqueDifferentiatorFilter;
	delete torqueObserverFilter;
}

double ControlBase::saturation(double in, double threshold)
{
    double sat;
    sat = max(in, -threshold);
    return min(sat, threshold);
}

double ControlBase::saturation(double in, double lowThreshold, double upThreshold)
{
    double  sat;
	sat = max(in, lowThreshold);
    return min( sat, upThreshold);
}

void ControlBase::refSaturation(double lowThreshold, double upThreshold)
{
	if(ref < lowThreshold)
    {
    	ref = lowThreshold;
    	dref = 0;
    	ddref = 0;
    }
	else if(ref > upThreshold)
    {
    	ref = upThreshold;
    	dref = 0;
    	ddref = 0;
    }
}


PID::PID(double ki, double kp, double kd)
{
	KP = kp;
	KD = kd;
	KI = ki;
    integralSAT = 10;
	outFilter = NULL;//DigitalFilter::get50HzFilter();
	velFilter = NULL;
}

double PID::_process(double pos, double vel, double deltaTime)
{
    err = ref - pos;
	derr = (dref - vel);
	if(velFilter != NULL) derr = velFilter->process(derr);
	ierr += err * deltaTime;
	ierr = saturation(ierr,integralSAT);
	out = KP*err + KD*derr + KI*ierr;
    if(outFilter != NULL) out = outFilter->process(out);
	return out;
}


MotorPID::MotorPID(double ki, double kp, double kd, ISEHardware* hw): MotorControlBase(hw)
{
	this->hw = hw;
	pid = new PID(ki,kp,kd);
}

double MotorPID::__process(double y, double dy, double dt)
{
	pid->ref = this->ref;
	pid->dref = this->dref;
	pid->ddref = this->ddref;
	return pid->process(y,dy,dt);
}

