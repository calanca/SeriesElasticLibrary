#include "DOBControl_CL.hpp"


/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Davide Ciocca, Andrea Calanca
 * @date Oct 2016
 ****************************************************************************/

DOBControl_CL::DOBControl_CL(double KP, double KD, double QcutoffHz, ISEHardware *hw) : DOBControlBase(KP, KD, QcutoffHz, hw)
{
    this->FF = 1;
    this->rd_prev = 0.0;

    // Q filter parameters
    double temp_a_Q[2] = {1.0, w_q};
    this->a_Q = temp_a_Q;
    double temp_b_Q[2] = {0.0, w_q};
    this->b_Q = temp_b_Q;
    this->Q_filter = new AnalogFilter(1, a_Q, b_Q);

    // PninvQ filter parameters
    double temp_a_PninvQ[3] = {KD, KP + FF + w_q * KD, (KP + FF) * w_q};
    this->a_PninvQ = temp_a_PninvQ;
    double temp_b_PninvQ[3] = {w_q * Jm / Kspring, w_q * (dm + de) / Kspring + KD, w_q * (1.0 + KP)};
    this->b_PninvQ = temp_b_PninvQ;
    this->PninvQ_filter = new AnalogFilter(2, a_PninvQ, b_PninvQ);

    this->name = "CL";
}

double DOBControl_CL::___process(double dt) {
    double rd;
    double Q_output, PninvQ_output;


    Q_output = Q_filter->process(rd_prev, dt);
    PninvQ_output = PninvQ_filter->process(tau, dt);
    d_hat = PninvQ_output - Q_output;


    rd = ref - 0.8*d_hat;
    rd_prev = rd;

    err = rd - tau;
    derr = dref - dtau;

    // PD Controller
    out = KP * err + KD * derr + rd * FF;

    return out;
}


DOBControl_CL3order::DOBControl_CL3order(double KP, double KD, double QcutoffHz, ISEHardware *hw) : DOBControlBase(KP, KD, QcutoffHz, hw) {

    this->FF = 0.0;

    this->rd_prev = 0.0;

    // Q filter parameters
    double mem_a_Q[2] = {1.0, w_q};
    this->a_Q = mem_a_Q;
    double mem_b_Q[2] = {0.0, w_q};
    this->b_Q = mem_b_Q;
    this->Q_filter = new AnalogFilter(1, a_Q, b_Q);

    // PninvQ filter parameters
    double mem_a_PninvQ[4] = {Kspring * JL * KD, Kspring * (w_q * JL * KD + JL * FF + JL * KP + de * KD), Kspring * (w_q * (JL * FF + JL * KP + de * KD) + de * (FF + KP)), Kspring * w_q * de * (FF + KP)};
    this->a_PninvQ = mem_a_PninvQ;
    double mem_b_PninvQ[4] = {w_q * FF * Jm * JL, w_q * (FF * (Jm * de + JL * dm) + Kspring * JL * KD), w_q * (FF * (Jm * Kspring + dm * de + JL * Kspring) + Kspring * (JL * KP + de * KD)), w_q * (Kspring * (FF * (dm + de) + de * KP))};
    this->b_PninvQ = mem_b_PninvQ;
    this->PninvQ_filter = new AnalogFilter(2, a_PninvQ, b_PninvQ);

    this->name = "CL3";
}

double DOBControl_CL3order::___process(double dt) {
    double rd;
    double Q_output, PninvQ_output;

    // Filter outputs:
    Q_output = Q_filter->process(rd_prev, dt);
    PninvQ_output = PninvQ_filter->process(tau, dt);
    d_hat = PninvQ_output - Q_output;

    rd = ref - 0.8*d_hat;
    rd_prev = rd;

    err = rd - tau;
    derr = dref - dtau;

    // PD Controller
    U = KP * err + KD * derr + rd * FF;

    return U;
}

