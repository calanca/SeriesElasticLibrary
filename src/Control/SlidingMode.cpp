#include "SlidingMode.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/


SlidingModeForceControlBase::SlidingModeForceControlBase(double lambda, ISEHardware* hw) : SEControl(hw)
{
	this->lambda1 = lambda;
	ddthetaEFilter = DigitalFilter::getLowPassFilterHz(20); //accelleration filter

	//taratura articolo
	phi = 20;
	ni = 1.0;

	ostringstream filenameStream;   // stream used for the conversion
	//filenameStream << "sliding" << "-lam" << lambda << "-ni"<< ni <<  "-phiVar" << phi << "-4Hz-main.csv";
	//filenameStream << "sliding" << "-lam" << lambda << "-ni"<< ni <<  "-phiVar" << "-rect-wood-hand.csv";
	filenameStream << "sliding" << ".csv";
	this->filename = filenameStream.str();
}

SlidingModeForceControl::SlidingModeForceControl(double lambda, ISEHardware* hw) : SlidingModeForceControlBase(lambda,hw)
{
	this->kp = -1;
	this->kd = A*lambda1; //IMPORTANT to be set in function of lambda
}


double SlidingModeForceControlBase::___process(double dt)
{
	cout << "time: " << time;

	err = tau - ref;
	derr = dtau - dref;
	s = slidingSurface(err,derr);
	out = controlLaw();
//	out += frictionCompensation(dtheta_m) * KT;
	return out;
}


double SlidingModeForceControl::slidingSurface(double err,double derr)
{
	s = derr + lambda1*err;
	return s;
}

double SlidingModeForceControl::controlLaw()
{
	//sliding strategy
	us = sign(s);
	//continuous approx
	if(fabs(s) < phi) us =  s / phi;

    out =  - kp*err - kd*derr + A*ddref + ref + Jm * ddtheta_e - ni*us;
//	out =  err - A*lambda1*derr + A*ddref + ref + Jm * ddtheta_ef - ni*us;
//	out =      - A*lambda1*derr + A*ddref + ref + Jm * ddtheta_ef - ni*us; //modified stabler version

	return out;
}


IntegralSlidingModeForceControl::IntegralSlidingModeForceControl(double l1, double l2, ISEHardware* hw): SlidingModeForceControlBase(l1,hw)
{
	lambda2 = l2;
	this->kp = A*lambda2 - 1; //IMPORTANT to be set in function of lambda12
	this->kd = A*lambda1; //IMPORTANT to be set in function of lambda12

	//-------------- resonant model --------------------
	damp = 0.997;
	freq = 1;
	res = DigitalFilter::getResonatorHz(freq,damp);

	//------------------------------------------------------
}

double IntegralSlidingModeForceControl::slidingSurface(double err,double derr)
{
    if(fabs(s) > 30) ierr = -(derr + lambda1*err)/lambda2;//to avoid the reaching phase

	ierr += err*TS;
	s = derr + lambda1*err + lambda2 * ierr;
	return s;
}

double IntegralSlidingModeForceControl::controlLaw()
{
	//sliding strategy
	us = sign(s);
	//continuous approx
	if(fabs(s) < phi)
	{
		us =  s / phi;
//		us =  s / phi + 0.15*res->process(s);
	}
    out = -kp*err -kd*derr + A*ddref + ref + 0.6 * Jm * ddtheta_e - ni*us;
    cout << " kp:" << kp;
	return out;
}


void SlidingModeForceControlBase::Log()
{
    logfile << this->time << " ";
    logfile << err << " ";
    logfile << derr << " ";
    logfile << ref << " ";
    logfile << dref << " ";
    logfile << phi << " ";
    logfile << ni << " ";
    logfile << s << " ";
    logfile << out << " ";
    logfile << endl;
}


// Super Twisting
SuperTwistingForceControl::SuperTwistingForceControl(double lambda, ISEHardware* hw) : SEControl(hw)
{
	ki = 5;
	ni = 0.15;
	rho = 0.5;

	a = Jm / Kspring;

	ddFilter = DigitalFilter::getLowPassFilterHz(100);

	this->ki = ki;
	this->ni = ni;
	this->rho = rho;
	this->lambda = lambda;

	this->filename = "superTW.csv";
}

double SuperTwistingForceControl::___process(double dt){

	err = tau - ref;
	derr = dtau - dref;
	s = derr + lambda*err;

	ddtheta_e = (dtheta_e - prev_dtheta_e) / dt;
	ddtheta_ef = ddFilter->process(ddtheta_e);
	prev_dtheta_e = dtheta_e;


	// control law
	dua = -ki * sign(s);
	ua += dua * dt;
	utw =  - ni * pow(fabs(s) , rho) * sign(s) + ua;

	out = a * (ddref - lambda * derr) + ref + Jm * ddtheta_ef + utw;

	cout << "time: " << time;


	return out;
}

void SuperTwistingForceControl::Log()
{
    logfile << this->time << " ";
    logfile << err << " ";
    logfile << derr << " ";
    logfile << ref << " ";
    logfile << dref << " ";
    logfile << 0 << " ";
    logfile << 0 << " ";
    logfile << s << " ";
    logfile << out << " ";
    logfile << endl;
}
