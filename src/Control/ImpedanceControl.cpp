#include "ImpedanceControl.hpp"
#include "defines.h"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

ImpedanceControlBase::ImpedanceControlBase(ISEHardware* hw) : SEControl(hw)
{
	this->filename = "impedance";
}

void ImpedanceControlBase::LogOpen()
{
    filenameStream << LOG_DIRECTORY << filename << floor(drand()*100) <<"---k_des_" << k_des << "-d_des_" << d_des << ".csv";
	string s(filenameStream.str());
	logfile.open(s.c_str());
}

void ImpedanceControlBase::Log()
{
	logfile << ref << " ";
	logfile << dref << " ";
	logfile << ddref << " ";
	logfile << tau << " ";
	logfile << dtau << " ";
	logfile << theta_m << " ";
	logfile << dtheta_m << " ";
	logfile << theta_e << " ";
	logfile << dtheta_e << " ";
	logfile << k_des << " ";
	logfile << d_des << " ";
    logfile << log1 << " ";
    logfile << log2 << " ";
    logfile << log3 << " ";
    logfile << this->time << " ";
    logfile << endl;
}


double TrivialImpedanceControl::___process(double dt)
{
//	tau = Jm*ddtheta_m - k_des*theta_m - d_des*dtheta_m + (k_des/K_SPRING)*torque + (d_des/K_SPRING)*dtorque;
	out = - k_des*theta_m - d_des*dtheta_m + (k_des/Kspring)*tau + (d_des/Kspring)*dtau;
	out += frictionTorqueCompensation(dtheta_m) * KT;
	// in pratica bisogna aggiungere attrito sul motore!!
	// si possono anche mettere i set points
	return out;
}



double BasicImpedanceControl::___process(double dt)
{
	//impedance control with inner force loop
	pid.ref =  - k_des * theta_e - d_des  * dtheta_e;
	pid.dref =  - k_des * dtheta_e - d_des * ddtheta_e;
	pid.ddref =  - k_des * ddtheta_e;
	out = pid.process(tau,dtau,dt);
	return out;
}

double CollocatedAdmittanceControl::___process(double dt)
{
    //collocated admittance control with inner force loop

    if(d_des > 0)
    {
        if(A_over_s_ref == NULL)
        {
            //double a[2] = {Kspring * d_des, Kspring * k_des};
            //double b[2] = {0, Kspring - k_des};
            double a[2] = {d_des, k_des};
            double b[2] = {0, 1 - k_des/Kspring};

            A_over_s_ref = new AnalogFilter(1,a,b);
            A_over_s_dref = new AnalogFilter(1,a,b);
        }

    pid.ref = - A_over_s_ref->process(tau,TS);
    pid.dref = - A_over_s_dref->process(dtau,TS);
    }
    else
    {

        pid.ref  =  - (Kspring - k_des)/(Kspring * k_des) * tau;
        pid.dref =  - (Kspring - k_des)/(Kspring * k_des) * dtau;
    }

    pid.ddref =  0;
	out = pid.process(theta_m,dtheta_m,dt);
	return out;
}

double CollocatedImpedanceControl::___process(double dt)
{
	//collocated impedance control with inner force loop
	k_des_m = (Kspring * k_des) / (Kspring - k_des);

	pid.ref =  - k_des_m * theta_m - d_des  * dtheta_m;
	pid.dref =  - k_des_m * dtheta_m - d_des * ddtheta_m;
	pid.ddref =  0;

	out = pid.process(tau,dtau,dt);
	return out;
}

AdaptiveImpedanceControl::AdaptiveImpedanceControl(ISEHardware* hw) : ImpedanceControlBase(hw)
{
    this->filename = "impedanceAD";
	double f = 20;
	double lambda2 = powf(2*M_PI*f,2);
	double lambda1 = 0.05*sqrt(lambda2);
	c = new MRAdaptiveForceControlBC(lambda1,lambda2,hw);
//	c = new IndirectAdaptiveForceControl(lambda1,lambda2,hw);
	c->L = 2*M_PI*20.0;
	c->setAdaptationSpeed(1);
//	c = new MultiAdaptiveForceControl(lambda1,lambda2,hw);
	c->referenceModelEnabled = false;
	c->adaptationEnabled = true;
	c->logEnabled = true;
    c->filename = "impedanceADforce.csv";

//	c->ni = 0.5; //migliori risultati tra 0.5 e 1
//	c->setAdaptationSpeed(50);
};

double AdaptiveImpedanceControl::___process(double dt)
{
	//impedance control with inner force loop
	c->ref =  - k_des * theta_e - d_des  * dtheta_e;
	c->dref =  - k_des * dtheta_e - d_des * ddtheta_e;
	c->ddref =  - k_des * ddtheta_e;

	out = c->process(tau,dtau,dt);
	return out;
}

SMImpedanceControl::SMImpedanceControl(ISEHardware* hw) : ImpedanceControlBase(hw)
{
	this->filename = "impedanceACC";
	double f = 20;
	double w = 2*M_PI*f;
	double lambda2 = powf(w,2);
	double lambda1 = 2.0*sqrt(lambda2);
	c = new IntegralSlidingModeForceControl(lambda1,lambda2,hw);
//	c = new SlidingModeForceControl(w,hw);
	c->ni = 0;
	c->phi = 5;
	c->logEnabled = false;
    c->torqueFromDisplacement = true;
}


double SMImpedanceControl::___process(double dt)
{
	//impedance control with inner force loop
	c->ref =  - k_des * theta_e - d_des  * dtheta_e;
	c->dref =  - k_des * dtheta_e - d_des * ddtheta_e;
	c->ddref =  - k_des * ddtheta_e;
//	c->ni = 0;
	out = c->process(tau,dtau,dt);
	return out;
}


VelocitySourcedImpedanceControl::VelocitySourcedImpedanceControl(ISEHardware* hw) : ImpedanceControlBase(hw)
{
    this->filename = "VSIC";
	c = new PassiveValleryForceControl(hw);
	c->logEnabled = false;
	logEnabled = false;
}


double VelocitySourcedImpedanceControl::___process(double dt)
{
	//impedance control with inner force loop
	c->ref =  - k_des * theta_e - d_des  * dtheta_e;
	c->dref =  - k_des * dtheta_e - d_des * ddtheta_e;
	c->ddref =  - k_des * ddtheta_e;
	cout << ref;
	out = c->process(tau,dtau,dt);
	return out;
}
