#include "IdentificationTask.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

LoopTask* id_create_task(ISEHardware* hw)
{

    #define LEN0 8
    double* amps = new double[8];
    double* freqs = new double[8];
    amps[0] = 8.0;//10
    freqs[0] = 1.0;
    amps[1] = 4.0;
    freqs[1] = 2.0;
    amps[2] = 3.0;
    freqs[2] = 4.0;
    amps[3] = 2.0;
    freqs[3] = 5.0;
    amps[4] = 1.5;
    freqs[4] = 6.0;
	amps[5] = 1.0;
    freqs[5] = 7.0;
	amps[6] = 1.0;
    freqs[6] = 8.0;
	amps[7] = 0.8;
    freqs[7] = 9.0;

	//    amps[8] = 0.8;
	//    freqs[8] = 10.0;
	//	  amps[9] = 0.8;
	//    freqs[9] = 11.0;
	//    amps[10] = 0.5;
	//    freqs[10] = 12.0;
	//	  amps[11] = 0.5;
	//    freqs[11] = 15.0;
	//    amps[12] = 0.4;
	//    freqs[12] = 20.0;



	//    amps[5] = 15.0;
	//    freqs[5] = 1.0;
	//    amps[6] = 20.0;
	//    freqs[6] = 0.5;
	//    amps[7] = 30.0;
	//    freqs[7] = 0.5;
	//    amps[8] = 40.0;
	//    freqs[8] = 0.2;
	//    amps[9] = 60.0;
	//    freqs[9] = 0.2;

    IdentificationTask* id = new IdentificationTask(amps,freqs,LEN0,hw);
    id->maxDuration = 360;
    id->filename = "Identification";

    #define LEN1 10
    double* amplitudes = new double[LEN1];


	amplitudes[0] = 10;
    amplitudes[1] = -10;
    amplitudes[2] = 8;
    amplitudes[3] = -8;
    amplitudes[4] = 6;
    amplitudes[5] = -6;
    amplitudes[6] = 4;
    amplitudes[7] = -4;
    amplitudes[8] = 2;
    amplitudes[9] = -2;


    FrictionIdentificationTask* fid = new FrictionIdentificationTask(amplitudes,LEN1,hw);
    string sf("Identification");
    fid->filename = sf;

    openLoopIdentificationTask* ol = new openLoopIdentificationTask(amps,freqs,LEN0,hw);
    string sol("sinCurrentLogLong");
    ol->filename = sol;

    return id;
}

IdentificationTask::IdentificationTask(double* amps, double* freqs, int n, ISEHardware* hw): LoopTask(hw)
{
    amp = amps;
    freq = freqs;
    size = n;
}

int IdentificationTask::_loop()
{
	if (idx >= size) return 0; //all experiments has been done*/

    if (hw->isStandStill(3333))
    {
        hw->resetM();
        hw->resetE();

		cout << "Experiment n." << idx << endl;

        pt = new SinPositionTask(hw);
        pt->amp = amp[idx]*1;
        pt->freq = freq[idx]*0.2;
        pt->logEnabled = true;
        pt->maxDuration = 10;

        pt->ctr->pid->KP = 1.0; // to keep the control lighter
        pt->filename = this->filename;

        //state concatenation
        pt->after = this;
        next = pt;
        //so after pt I come here again

        idx++;
    }
    return 1;
}


int FrictionIdentificationTask::_loop()
{
	if (idx >= size) return 0; //all experiments has been done*/

    if (hw->isStandStill(3333))
    {
        hw->resetM();
        hw->resetE();

		cout << "Experiment n." << idx << endl;

//        ct = new CurrentTask(hw);
//		  ct->current_ref= amp[idx]*0.4;
//        cout << "Current Ref = " << ct->current_ref << endl;
//        ct->logEnabled = true;
//        ct->maxDuration = 25;
//        ct->filename = this->filename;
//        //state concatenation
//        ct->after = this;
//        next = ct;
//        //so after pt I come here again


        vt = new VelocityTask(hw);
		vt->velocity_ref= amp[idx]*15;//to tune;
        vt->logEnabled = true;
        vt->maxDuration = 20;
        vt->filename = this->filename;
        //state concatenation
        vt->after = this;
        next = vt;
        //so after pt I come here again



        idx++;

    }
    return 1;
}


int openLoopIdentificationTask::_loop()
{
    if (hw->isStandStill(3333))
    {
        hw->resetM();
        hw->resetE();

        t = new SinCurrentTask(hw);
		t->amp = amp[idx];
        t->freq = freq[idx];
        t->logEnabled = true;
        t->maxDuration = 7;
        t->filename = this->filename;

        //state concatenation
        t->after = this;
        next = t;
        //so after pt I come here again

        cout << "Experiment n." << idx;

        idx++;

    }
    if (idx > size) return 0; //all experiments has been done*/
    return 1;
}


