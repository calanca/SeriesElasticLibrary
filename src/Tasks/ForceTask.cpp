#include "ForceTask.hpp"


LoopTask* f___create_task(ISEHardware* hw)
{
    ForceTask* ft = new ForceTask(hw);
    ft->logEnabled = true;
	ft->maxDuration = 60;

	return ft;
}


//faulhaber
//#define KP 14.0 // observer
#define KP 100.0
#define KD 0
#define KI 0.0

ForceTask::ForceTask(ISEHardware* hw): LoopTask(hw)
{
    pd = new MotorPID(KI,KP,KD,hw);
    pd->pid->velFilter = DigitalFilter::getLowPassFilterHz(5);

	double lambda1, lambda2, f;
	f = 1;
	lambda2 = powf(2*M_PI*f,2);
	lambda1 = 2.0*sqrt(lambda2);
    mrac = new KEAdaptiveForceControl(hw);
    mrac->logEnabled = true;

    kac = new KAdaptiveForceControl(hw);
    kac->logEnabled = true;
    kac->adaptationEnabled = true;
	kac->torqueFromDisplacement = false;
//    mrac->setAdaptationSpeed(10);
//    mrac->L = 2*M_PI*20.0;

	dob = new DOBForceControl(hw);
	dob->logEnabled = true;

    filename = "force";

    ctr = pd;


}

void ForceTask::LogOpen()
{
	filenameStream << filename  << "-ref_"<< ctr->ref << ".csv";
	string s(filenameStream.str());
	logfile.open(s.c_str());
}

int ForceTask::_loop()
{
//    ctr->ref = 0;
//    ctr->dref = 0;
//    ctr->ddref = 0;

//	//SIN
//    double amp = 0.5;
//    double freq = 2;
//	ctr->ref = amp*sin(2 * M_PI * freq * time);
//	ctr->dref =  amp* 2 * M_PI * freq * cos(2 * M_PI * freq * time);
//	ctr->ddref =  - amp * powf(2 * M_PI * freq,2) * sin(2 * M_PI * freq * time);

//	k_des = 0.5;
//	d_des = 0.001;
//	ctr->ref =  - k_des * ctr->theta_m - d_des  * ctr->dtheta_m;
//	ctr->dref =  - k_des * ctr->dtheta_m - d_des * ctr->ddtheta_m;
//	ctr->ddref =  - k_des * ctr->ddtheta_m;

    current = ctr->ref + ctr->process(ctr->tau,ctr->dtau,dt) / KT;
//	current = 0;
//	current = ctr->process(ctr->torqueObserver,ctr->diffTorqueObserver,dt) / KT;
//    current += ctr->frictionCurrentCompensation(hw->getDThetaM());
//    current += -0.2 * hw->getThetaM() / KT;
    hw->setCurrent(current);
    return 1;
}


void ForceTask::Log()
{
    logfile << time << " ";
    logfile << hw->getCurrent() << " ";
    logfile << current << " ";
    logfile << hw->getThetaM() << " ";
    logfile << hw->getDThetaM() << " ";
    logfile << hw->getTorque() << " ";
    logfile << hw->getDiffTorque() << " ";
    logfile << ctr->ref << " ";
    logfile << ctr->dref << " ";
    logfile << ctr->torqueObserver << " ";
    logfile << ctr->diffTorqueObserver << " ";
    logfile << log1 << " ";
    logfile << log2 << " ";
    logfile << log3 << " ";
    logfile << endl;
}
