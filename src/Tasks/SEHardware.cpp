#include <iostream>
#include <stdio.h>
#include "SEHardware.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/



SEHardware::SEHardware(double Jm, double Kspring, double Kt)
{
	this->Jm = Jm;
	this->Kspring = Kspring;
	this->Kt = Kt;

	this->coulombFrictionP = 0;
	this->coulombFrictionN = 0;
	this->viscousFrictionP = 0;
	this->viscousFrictionN = 0;

	srand (time(NULL));
	toRadians = 1.0/(double)ENCODER_STEPS*2*M_PI;
	prev_torque = 0;

	envelopeFilterM = DigitalFilter::getLowPassFilterHz(1);
	envelopeFilterE = DigitalFilter::getLowPassFilterHz(1);

	sheepBoard = new SheepBoard("SheepBoard");
    //this method is supposed to configure the communication with the etercat hardware
    sheepBoard->configureHook();
    setCurrent(0.0);

    this->zeroMOTOR = 0;
    this->zeroLOAD = 0;
}

double SEHardware::curSaturation(double in)
{
    if(fabs(in) > currentSaturation)
    {
		cout<<"SATURATION!!";
		saturation_flag = true;
		return currentSaturation*sign(in);
    }
   else
   {
   	saturation_flag = false;
   	return in;
   }
}


void SEHardware::refresh(double dt)
{
    //torque/current
    active_current = (double)sheepBoard->analogSensorIn[0]*READ_CURRENT_CONVERSION; // current reference
    active_tau_m = active_current*KT; // torque reference

    torque = (double)sheepBoard->analogSensorIn[1]*READ_TORQUE_CONVERSION - TORQUE_SENSOR_OFFSET;
    difftorque =  (torque - prev_torque) / dt;

    //encoder position in rad
    thetaM = -(double)(sheepBoard->encodersPositionIn[MOTOR]-zeroMOTOR)*toRadians; //encoders
    thetaE = -(double)(sheepBoard->encodersPositionIn[LOAD]-zeroLOAD)*toRadians; //encoders


    //encoder velocity in rad
    //1. diff velocity
    diffthetaM = (double)(-sheepBoard->encodersPositionIn[MOTOR] + prev_encoderMOTOR) * (toRadians / dt); //it is better to make a int difference!!
    diffthetaE = (double)(-sheepBoard->encodersPositionIn[LOAD] + prev_encoderLOAD) * (toRadians / dt); //it is better to make a int difference!!

    //2.period velocity

    //if the position is about the same the period is incremented -> about means within 1e-10 (can be tuned)
    //otherwise the new period value is read and converted in seconds

    if(fabs(thetaM-prev_thetaM) < 1e-10) periodM += dt;
    else periodM = 0.25e-7 * sheepBoard->encodersPeriodIn[MOTOR]; // //encoder period in second: 4 step per pulse * 1e-7 time conversion

    if(fabs(thetaE-prev_thetaE) < 1e-10) periodE += dt;
    else periodE = 0.25e-7 * sheepBoard->encodersPeriodIn[LOAD]; // //encoder period in second: 4 step per pulse * 1e-7 time conversion

    //sign
    if(diffthetaM != 0) signM = diffthetaM > 0 ? 1.0 :-1.0;
    if(diffthetaE != 0) signE = diffthetaE > 0 ? 1.0 :-1.0;
    period_dthetaM = signM * (2*M_PI / ENCODER_STEPS) / periodM;
    period_dthetaE = signE * (2*M_PI / ENCODER_STEPS) / periodE;

//  //this results in chattering because the sign of 0 is positive!
//  period_dthetaM = sign(diffthetaM) * (2*M_PI / ENCODER_STEPS) / periodM;
//	period_dthetaE = sign(diffthetaE) * (2*M_PI / ENCODER_STEPS) / periodE;

    //sometimes I read spikes in the velocity
	//outlier removal 1
	//sudden increments > 10 [rad/s] are skipped and I get slowly closer to the period value
	//se ho un incremento improvviso > 10 lo cazzio subito. e mi avvicino lentamente al nuovo valore di period

	dthetaM = period_dthetaM;
	dthetaE = period_dthetaE;

	inc_dthetaM = fabs(dthetaM-prev_dthetaM);
	inc_dthetaE = fabs(dthetaE-prev_dthetaE);

    if( inc_dthetaM > 1) {dthetaM = prev_dthetaM + sign(period_dthetaM-prev_dthetaM); cout << "velocity_outlier_M";}
    if( inc_dthetaE > 1) {dthetaE = prev_dthetaE + sign(period_dthetaE-prev_dthetaE); cout << "velocity_outlier_E";}

	//outlier removal 2 (alternative outlier removal based on envelope - works worse)
//	dthetaM_envelope = envelopeFilterM->process(fabs(prev_dthetaM));
//  dthetaE_envelope = envelopeFilterE->process(fabs(prev_dthetaE));
//	if( inc_dthetaM > 0.5*dthetaM_envelope) dthetaM = prev_dthetaM + sign(period_dthetaM-prev_dthetaM);
//	if( inc_dthetaE > 0.5*dthetaE_envelope) dthetaE = prev_dthetaE + sign(period_dthetaE-prev_dthetaE);

//	//artificial noise (just for test)
//	i++;
//	rand_amp = 0.0;
//	randM = drand()*rand_amp - rand_amp/2;
//	randE = drand()*rand_amp - rand_amp/2;
//	dthetaM += randM;
//	dthetaE += randE;

	//check if we are at stand still
	if(fabs(thetaM-prev_thetaM) < 1e-10) isStandStillM += dt;
	else isStandStillM = 0;

	if(fabs(thetaE-prev_thetaE) < 1e-10) isStandStillE += dt;
	else isStandStillE = 0;

	//acceleration
	ddiffthetaM = (dthetaM - prev_dthetaM) / dt;
	ddiffthetaE = (dthetaE - prev_dthetaE) / dt;

    //updates
	prev_encoderMOTOR = sheepBoard->encodersPositionIn[MOTOR];
	prev_encoderLOAD = sheepBoard->encodersPositionIn[LOAD];

	prev_thetaM = thetaM;
	prev_thetaE = thetaE;

	prev_dthetaM = dthetaM;
	prev_dthetaE = dthetaE;

	prev_torque = torque;
}


bool SEHardware::isStandStill(int time)
{
    if(isStandStillM>time && isStandStillE>time) return true;
    else return false;
}
