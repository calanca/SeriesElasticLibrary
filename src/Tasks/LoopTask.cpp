#include "LoopTask.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

LoopTask::LoopTask(ISEHardware* hw)
{
    this->hw = hw;
    next = this;
    maxDuration = 60.0; ///< task max duration in seconds, 1 min by default
    endLine = true;
}

int LoopTask::loop(double dt)
{
	next = this; //by default this is a loop task!!

    this->time += dt;
    this->dt = dt;
    hw->refresh(dt);

	if (logEnabled && !logfile.is_open()) LogOpen();

	//do action
    toRet =  this->_loop();

	//log
	if (logEnabled) Log();

	//next transition
	goToNext();

	//after transition
	if (isFinished())
    {
    	goToAfter();
    	cout<< "Task Finished: time " <<time<<endl;
        hw->setCurrent(0.0);

    }

    //the loop cout - however it is not true that all task should have a cout
    if(endLine) cout << endl;

    return toRet;
}

LoopTask *LoopTask::getCurrentTask() { return currentTask;}
void LoopTask::setCurrentTask(LoopTask* t) { currentTask = t;}

void LoopTask::goToNext()
{
	if(next == NULL) toRet = 0;
	else
	{
		if(currentTask!=next) { currentTask->dispose(); next->timeReset(); }
		currentTask = next;
	}
}

void LoopTask::goToAfter()
{
	if(after == NULL) toRet = 0;
	else
	{
		currentTask->dispose();
		after->timeReset();
		currentTask = after;
	}
}



void LoopTask::LogCurPos()
{
    logfile << time << " ";
    logfile << hw->getCurrent() << " ";
    logfile << current << " ";
    logfile << hw->getThetaM() << " ";
    logfile << hw->getDiffThetaM() << " ";
    logfile << hw->getDThetaM() << " ";
    logfile << hw->getThetaE() << " ";
    logfile << hw->getDiffThetaE() << " ";
    logfile << hw->getDThetaE() << " ";
    logfile << endl;
}

void LoopTask::LogAll()
{
    logfile << time << " ";
    logfile << hw->getCurrent() << " ";
    logfile << current << " ";
    logfile << hw->getThetaM() << " ";
    logfile << hw->getDiffThetaM() << " ";
    logfile << hw->getDThetaM() << " ";
    logfile << hw->getThetaE() << " ";
    logfile << hw->getDiffThetaE() << " ";
    logfile << hw->getDThetaE() << " ";
    logfile << hw->getTorque() << " ";
    logfile << endl;
}

void LoopTask::logMotor()
{
    logfile << time << " ";
    logfile << hw->getCurrent() << " ";
    logfile << hw->getThetaM() << " ";
    logfile << hw->getDiffThetaM() << " ";
    logfile << hw->getDThetaM() << " ";
    logfile << endl;
}


