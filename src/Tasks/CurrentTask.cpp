#include "CurrentTask.hpp"
#include "ControlBase.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
****************************************************************************/

LoopTask* ____cu__create_task(ISEHardware* hw)
{
    CurrentTask* c = new CurrentTask(hw);
    c->logEnabled = true;
    c->current_ref = -0.2;

  	SinCurrentTask* sc = new SinCurrentTask(hw);
    sc->amp = 0.5;
    sc->freq = 0.5;
    sc->maxDuration = 20.0;
    sc->logEnabled = true;

	return c;
}

void CurrentTask::LogOpen()
{
	filenameStream << filename << "-cur_ref_" << current_ref << ".csv";
	string s(filenameStream.str());
	logfile.open(s.c_str());
}

int CurrentTask::_loop()
{
    current = current_ref;
	//current += Control::frictionCompensation(hw->getDThetaM());
    hw->setCurrent(current);
    cout << hw->getTorque();

    return 1;
}

void SinCurrentTask::LogOpen()
{
    filenameStream << filename << "-cur_ref" << amp << "sin"<< freq << "t.csv";
	string s(filenameStream.str());
	logfile.open(s.c_str());
}

int SinCurrentTask::_loop()
{
    current =  amp * sin(2 * M_PI * freq * time);
	//current += Control::frictionCompensation(thetaMfilter->process(hw->getDiffThetaM()));
    hw->setCurrent(current);
    return 1;
}
