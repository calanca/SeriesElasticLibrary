#include "PositionTask.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

LoopTask* pt____create_task(ISEHardware* hw)
{
    SinPositionTask* spt = new SinPositionTask(hw);
    spt->amp = 3.14/4;
    spt->freq = 5;
    spt->logEnabled = true;
    spt->maxDuration = 20.0;

	PositionTask* pt = new PositionTask(hw);
    pt->logEnabled = true;
	pt->maxDuration = 600;

	VelocityTask* vt = new VelocityTask(hw);
	vt->velocity_ref = 5;
	vt->logEnabled = true;
	vt->maxDuration = 10;

	HomingTask* ht = new HomingTask(hw);

	return pt;
}

//JPL
//#define KP 5.0
//#define KD 0.2
//#define KI 0.0

//faulhaber
#define KP 2.0
#define KD 0.04
#define KI 0.0

//#define KP 0.05
//#define KD 0.001
//#define KI 0.0

//
//#define KP 40.0
//#define KD 2
//#define KI 1



PositionTask::PositionTask(ISEHardware* hw): LoopTask(hw)
{
    ctr = new MotorPID(KI,KP,KD,hw);
    filename = "position";
}

void PositionTask::LogOpen()
{
    filenameStream << LOG_DIRECTORY << filename  << "-posref_"<< ctr->ref << "_3Khz_only_enc_outliers_1.csv";
	string s(filenameStream.str());
	logfile.open(s.c_str());
}

int PositionTask::_loop()
{
    ctr->ref = position_ref;
    current = ctr->process(hw->getThetaM(),hw->getDThetaM(),dt) / KT;
//    current += ctr->frictionCurrentCompensation(hw->getDThetaM());
    cout << hw->getDThetaM();
    hw->setCurrent(current);
    return 1;
}

VelocityTask::VelocityTask(ISEHardware* hw): LoopTask(hw)
{
    ctr = new MotorPID(KP,KD,0,hw);
    filename = "velocity";
}

void VelocityTask::LogOpen()
{
    filenameStream << LOG_DIRECTORY << filename  << "-velref_"<<  velocity_ref << ".csv";
	string s(filenameStream.str());
	logfile.open(s.c_str());
}

int VelocityTask::_loop()
{
	if(time < M_PI) ctr->ref = velocity_ref*sin(2*M_1_PI*time);
	else ctr->ref = velocity_ref;
	current = ctr->process(hw->getDThetaM(),hw->getDDiffThetaM(),dt) / KT;
	//current += - 0.001*hw->getDiffTorque(); //to damp spring oscillations
    hw->setCurrent(current);
    return 1;
}

void SinPositionTask::LogOpen()
{
    filenameStream << LOG_DIRECTORY << filename << "-posref_"<< amp << "sin"<< freq << "t.csv";
	string s(filenameStream.str());
	logfile.open(s.c_str());
}

int SinPositionTask::_loop()
{
    fadeIn = fadeIn < 1.0 ? time : 1.0;
    ctr->ref =  fadeIn * amp * sin(2 * M_PI * freq * time);
    ctr->dref = fadeIn * amp * freq * cos(2 * M_PI * freq * time);
    ctr->ddref = fadeIn * (-amp) * freq * freq * sin(2 * M_PI * freq * time);

    current = ctr->process(hw->getThetaM(),hw->getDThetaM(),dt) / KT;

	hw->setCurrent(current);

    return 1;
}

int HomingTask::_loop()
{
    if (hw->getIndexM())
    {
        hw->resetM();
        cout << "home!";
        return 0;
    }
    else
    {
        ctr->ref = time*M_PI*0.2;
        current = ctr->process(hw->getThetaM(),hw->getDThetaM(),dt) / KT;
        hw->setCurrent(current);
        return 1;
    }
}
