#include "SEForceTask.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/


LoopTask* create_task(ISEHardware* hw)
{
    SEForceTask* tc = new SEForceTask(hw);
    tc->filename = "SEForceTaskLog";
    tc->maxDuration = 25.0;
    tc->logEnabled = false;
    tc->torque_ref = 0.01;
    return tc;
}


SEForceTask::~SEForceTask()
{
	delete mrac;
	delete mrpd;
    delete pd;
	delete ism;
	delete stw;
	delete imp;
	delete BIC;
	delete MRACimp;
	delete SMimp;
	delete passivePid;
	delete passivePratt;
	delete mrPassivePratt;
	delete passiveVallery;
	delete indirectAdaptive;
	delete VSIC;
    delete OL_dob;
    delete CL_dob;
    delete OL_dob_3_order;
    delete CL_dob_3_order;
}

void SEForceTask::init(double lambda1, double lambda2)
{
	//-------- adaptive ---------------------------
    mrac = new MRAdaptiveForceControlBC(lambda1,lambda2,hw);
    mrac->referenceModelEnabled = true;
    mrac->L = 2*M_PI*20.0;
    mrac->setAdaptationSpeed(1);
    mrac->ni = 0.5; //migliori risultati tra 0.5 e 1
	mrac->logEnabled = true;
    mrac->ka = 0.0;

	mrpd = new MRPDControl(lambda1,lambda2,hw);
	mrpd->kp = 30;
    mrpd->kd = 0.01; //0.1
    mrpd->ki = 0; //1.0
	mrpd->logEnabled = true;

    indirectAdaptive = new IndirectAdaptiveForceControlBC(lambda1,lambda2,hw);
	indirectAdaptive->logEnabled = true;
	indirectAdaptive->ni = 0.0; //migliori risultati tra 0.5 e 1
	indirectAdaptive->setAdaptationSpeed(10);
	indirectAdaptive->gradientUpdate = true;


    MMadaptive = new MultiAdaptiveForceControlBC(lambda1,lambda2,hw);
	MMadaptive->logEnabled = true;

	//-----sliding-modes ---------------

	sm = new SlidingModeForceControl(lambda1,hw);
    sm->ni = 0.0;
	sm->logEnabled = true;

	ism = new IntegralSlidingModeForceControl(lambda1,lambda2,hw);
    ism->ni = 4;
	ism->phi = 10;
	ism->logEnabled = true;

    stw = new SuperTwistingForceControl(lambda1,hw);
	stw->ni = 0.2;
	stw->logEnabled = true;

	//---------Passive-----------------------------------

	passivePid = new PassivePIDControl(hw);
    passivePid->logEnabled = true;
    passivePid->kp = 20; // PD tuning in environmentt acceleration
    passivePid->kd = 0.3; // PD tuning in environmentt acceleration
//    passivePid->kp = 50; // PD tuning in environmentt acceleration
//    passivePid->kd = 1.0; // PD tuning in environmentt acceleration
    passivePid->ki = 0.0; // PD tuning in environmentt acceleration
    passivePid->ff = 1.0; // PD tuning in environmentt acceleration
    passivePid->filename = "PD_kp20.csv";
//    passivePid->filename = "PassivePID_kp20.csv";

	passivePratt = new PassivePrattForceControl(lambda1,lambda2,hw);
	passivePratt->logEnabled = true;
	//	passivePratt->kb = 1.0;
	passivePratt->kb = 0.9;


	mrPassivePratt = new MRPassivePrattForceControl(lambda1,lambda2,hw);
	mrPassivePratt->logEnabled = true;
	mrPassivePratt->kb = 0.9;

	passiveVallery = new PassiveValleryForceControl(hw);
	passiveVallery->logEnabled = true;

	//----------Impedance----------------------------------

	imp = new TrivialImpedanceControl(hw);
	imp->logEnabled = true;

	BIC = new BasicImpedanceControl(hw);
	BIC->logEnabled = true;

	MRACimp = new AdaptiveImpedanceControl(hw);
	MRACimp->logEnabled = true;

	SMimp = new SMImpedanceControl(hw);
	SMimp->logEnabled = true;

	VSIC = new VelocitySourcedImpedanceControl(hw);
	VSIC->logEnabled = true;

	CIC = new CollocatedImpedanceControl(hw);
	CIC->logEnabled = true;

	CAC = new CollocatedAdmittanceControl(hw);
	CAC->logEnabled = true;

    //----------DOB----------------------------------

    double QcutoffHz = 2; //Hz
    double dm = 0;
    double w_closedloop = sqrt(lambda2);
    double xi_closedloop = 1.4;
    double KP = (JM / K_SPRING) * lambda2 - 1;
    double KD = 2.0 * (xi_closedloop / w_closedloop) * (KP+1) - (dm / K_SPRING);

    pd = new PassivePIDControl(hw);
    pd->logEnabled = true;
    pd->kp = KP; // PD tuning in environmentt acceleration
    pd->kd = KD; // PD tuning in environmentt acceleration
    pd->ki = 0.0; // PD tuning in environmentt acceleration
    pd->ff = 1.0; // PD tuning in environmentt acceleration
    pd->filename = "PD.csv";
//    passivePid->filename = "PassivePID_kp20.csv";

    OL_dob = new DOBControl_OL(KP, KD, QcutoffHz, hw);
    OL_dob->dynamicFFEnabled = true;
    OL_dob->logEnabled = true;

    CL_dob = new DOBControl_CL(KP, KD, QcutoffHz, hw);;
    CL_dob->logEnabled = true;

    OL_dob_3_order = new DOBControl_OL3order(KP, KD, QcutoffHz, hw);;
    OL_dob_3_order->dynamicFFEnabled = true;
    OL_dob_3_order->logEnabled = true;

    CL_dob_3_order = new DOBControl_CL3order(KP, KD, QcutoffHz, hw);;
    CL_dob_3_order->logEnabled = true;
}

SEForceTask::SEForceTask(ISEHardware* hw): LoopTask(hw)
{
	//----- tuning ---------------
    // performance parameters: define the (second order) reference model for the closed loop system
	double lambda1, lambda2, f;
    f = 10;
    lambda2 = powf(2*M_PI*f,2);
	lambda1 = 2.0*sqrt(lambda2); //diminuire questo toglie il chattering

	init(lambda1,lambda2);

    //------- set the active controller and reference ------------

////  Force Control
  ctr = ism;
//  ctr = sm;
//  ctr = passivePratt;
//	ctr = mrPassivePratt;
//    ctr = passivePid;
//  ctr = mrac;
//  ctr = pd;
//  ctr = OL_dob;
//  ctr = CL_dob;
//  ctr = OL_dob_3_order;
//  ctr = CL_dob_3_order;
//	ctr = mrpd;
//  ctr = imp;
//	ctr = indirectAdaptive;
//	ctr = MMadaptive;
//	ctr = passiveVallery;

//// Impedance Control
//  ctr = MRACimp;
//    ctr = VSIC;
//    ctr = BIC;
//    ctr = CAC;
//    ctr = CIC;
//    ctr = SMimp;
//    SMimp->c->ni = 0.0;

    k_des = 0.3 * K_SPRING;
    d_des = 0.0;

	//set desired impedance
	imp->k_des = k_des;
	imp->d_des = d_des;
	BIC->k_des = k_des;
	BIC->d_des = d_des;
	MRACimp->k_des = k_des;
	MRACimp->d_des = d_des;
	SMimp->k_des = k_des;
	SMimp->d_des = d_des;
	VSIC->k_des = k_des;
	VSIC->d_des = d_des;
	CIC->k_des = k_des;
	CIC->d_des = d_des;
	CAC->k_des = k_des;
	CAC->d_des = d_des;

//	ctr->torqueFromDisplacement = false;
//	ctr->dtorqueFilter = DigitalFilter::getLowPassFilterHz(5);

	ctr->torqueFromDisplacement = true;


}


int SEForceTask::_loop()
{

	//SIN
    amp = 0.15;

//    freq = 2;
//	ctr->ref = amp + amp*sin(2 * M_PI * freq * time);
//	ctr->dref =  amp* 2 * M_PI * freq * cos(2 * M_PI * freq * time);
//	ctr->ddref =  - amp * powf(2 * M_PI * freq,2) * sin(2 * M_PI * freq * time);

    // Paper EnvironmentAdaptive
//    ctr->ref = amp * (2 + sin(2*M_PI*time) + 0.2*sin(4*M_PI*time) + 0.1*sin(10*M_PI*time));
//    ctr->dref =  amp * (2*M_PI*cos(2*M_PI*time) + 0.2*4*M_PI*cos(4*M_PI*time) + 0.1*10*M_PI*cos(10*M_PI*time));
//    ctr->ddref =  - amp * (4*M_PI*M_PI*sin(2*M_PI*time) + 0.2*16*M_PI*M_PI*sin(4*M_PI*time) + 0.1*100*M_PI*M_PI*sin(10*M_PI*time));

//     Paper EnvironmentAcceleration
//    ctr->ref = amp * (2 + sin(2*M_PI*time) + 0.2*cos(4*M_PI*time) + 0.1*sin(10*M_PI*time));
//    ctr->dref =  amp * (2*M_PI*cos(2*M_PI*time) - 0.2*4*M_PI*sin(4*M_PI*time) + 0.1*10*M_PI*cos(10*M_PI*time));
//    ctr->ddref =  - amp * (4*M_PI*M_PI*sin(2*M_PI*time) + 0.2*16*M_PI*M_PI*cos(4*M_PI*time) + 0.1*100*M_PI*M_PI*sin(10*M_PI*time));

//    //TEST DAVIDE
//    //Ref_1
    ctr->ref = amp * (2 + sin(2 * M_PI * time) + 0.2 * sin(4 * M_PI * time) + 0.1 * sin(10 * M_PI * time));
    ctr->dref = amp * (2 * M_PI * cos(2 * M_PI * time) + 0.2 * 4 * M_PI * cos(4 * M_PI * time) + 0.1 * 10 * M_PI * cos(10 * M_PI * time));
    ctr->ddref = - amp * (4 * M_PI * M_PI * sin(2 * M_PI * time) + 0.2 * 16 * M_PI * M_PI * sin(4 * M_PI * time) + 0.1 * 100 * M_PI * M_PI * sin(10 * M_PI * time));

    // Ref_2
//    ctr->ref = amp * (0.5 + sin(2 * M_PI * time) + 0.5 * sin(4 * M_PI * time) + 0.25 * sin(10 * M_PI * time));
////    ctr->dref = amp * (2 * M_PI * cos(2 * M_PI * time) + 0.5 * 4 * M_PI * cos(4 * M_PI * time) + 0.25 * 10 * M_PI * cos(10 * M_PI * time));
//    ctr->ddref = - amp * (4 * M_PI * M_PI * sin(2 * M_PI * time) + 0.5 * 16 * M_PI * M_PI * sin(4 * M_PI * time) + 0.25 * 100 * M_PI * M_PI * sin(10 * M_PI * time));

////	STEP
//    ctr->ref = 1;
//    ctr->dref = 0;
//    ctr->ddref = 0;

////  ZERO FORCE MODE
//    ctr->ref = 0;
//    ctr->dref = 0;
//    ctr->ddref = 0;

//	k_des = 1;
//	d_des = 0.00;

//	ctr->ref =  - k_des * ctr->theta_m - d_des  * ctr->dtheta_m;
//	ctr->dref =  - k_des * ctr->dtheta_m - d_des * ctr->ddtheta_m;
//	ctr->ddref =  - k_des * ctr->ddtheta_m;

//    current = ctr->process(0.0,0.0,dt) / KT; // SEA controllers already know tau & dtau
//    current += ctr->frictionCurrentCompensation();
//    cout << "       " <<current;
//    cout << ctr->tau << endl;
//    hw->setCurrent(current); //TODO i don t like to call it current.. set the torque instead!!


    double tauM;
    tauM = ctr->process(0.0,0.0,dt); // SEA controllers already know tau & dtau
    tauM += ctr->frictionTorqueCompensation();
    hw->setTauM(tauM);
    return 1;
}

void SEForceTask::LogTorque()
{
    logfile << time << " ";
    logfile << hw->getCurrent() << " ";
    logfile << current << " ";
    logfile << endl;
}
